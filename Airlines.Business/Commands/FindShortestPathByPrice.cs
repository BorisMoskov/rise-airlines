﻿using Airlines.Business.Models;
using AirlinesBusiness;
using System.Diagnostics.CodeAnalysis;
using System.IO;

namespace Airlines.Business.Commands;
[ExcludeFromCodeCoverage]
internal class FindShortestPathByPrice(Graph<string> flightGraph, string startAirport, string endAirport, LinkedList<Flight> flights) : ICommand
{
    private readonly Graph<string> _flightGraph = flightGraph;
    private readonly string _startAirport = startAirport;
    private readonly string _endAirport = endAirport;
    private readonly LinkedList<Flight> _flights = flights;

    public void Execute()
    {
        Dictionary<string, double> prices = [];
        Dictionary<string, string> parentMap = [];
        HashSet<string> visited = [];
        List<string> path;

        // Initialize prices to airports with infinity
        foreach (var node in _flightGraph.GraphNodes)
        {
            prices[node.Data] = double.PositiveInfinity;
        }

        // Initialize the start airport price to 0
        prices[_startAirport] = 0;

        // Initialize priority queue for Dijkstra's algorithm
        PriorityQueue<Tuple<string, double>, double> queue = new PriorityQueue<Tuple<string, double>, double>();
        queue.Enqueue(new Tuple<string, double>(_startAirport, 0), 0);

        while (queue.Count > 0)
        {
            var (currentAirport, _) = queue.Dequeue();

            if (visited.Contains(currentAirport)) continue;

            _ = visited.Add(currentAirport);

            foreach (var flight in _flights)
            {
                if (flight.DepartureAirport == currentAirport)
                {
                    var neighbour = flight.ArrivalAirport;
                    double newPrice = flight.Price;

                    if (newPrice < prices[neighbour])
                    {
                        prices[neighbour] = newPrice;
                        parentMap[neighbour] = currentAirport;
                        queue.Enqueue(new Tuple<string, double>(neighbour, newPrice), newPrice);
                    }
                }
            }
        }

        // Reconstruct the shortest path
        path = [];
        if (prices.TryGetValue(_endAirport, out var value) && !double.IsPositiveInfinity(value))
        {
            string airport = _endAirport;
            while (airport != _startAirport)
            {
                path.Add(airport);
                airport = parentMap[airport];
            }
            path.Add(_startAirport);
            path.Reverse();
            PrintInformation.PrintRoutePathByPrice(true, path, prices); // Path found
        }
        else
        {
            PrintInformation.PrintRoutePathByPrice(false, null!, prices); // No path found
        }
    }
}