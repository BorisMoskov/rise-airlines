﻿using Airlines.Business.Models;
using System.Diagnostics.CodeAnalysis;

namespace Airlines.Business.Commands;
[ExcludeFromCodeCoverage]
public class FlightRouteSearchCommand(FlightRouteTree flightRouteTree) : ICommand
{
    private readonly FlightRouteTree _flightRouteTree = flightRouteTree;
    public string? DestinationAirport { get; set; }

    public void Execute()
    {
        if (string.IsNullOrWhiteSpace(DestinationAirport))
        {
            Console.WriteLine("Destination airport is not set.");
            return;
        }

        var route = _flightRouteTree.FindRoute(DestinationAirport);
        if (route.Count > 0)
        {
            Console.WriteLine($"Route to {DestinationAirport}:");
            for (int i = 0; i < route.Count - 1; i++)
            {
                Console.Write($"{route[i]} -> ");
            }
            Console.WriteLine(route[^1]); // Print the last airport without "->"
        }
        else
        {
            Console.WriteLine($"No route to {DestinationAirport} found.");
        }
    }

    // Print the flight route tree
    public void PrintTree()
    {
        Console.WriteLine("Flight Route Tree:");
        PrintTreeRecursive(_flightRouteTree.Root, 0);
    }

    private static void PrintTreeRecursive(TreeNode<string> node, int depth)
    {
        if (node == null)
        {
            return;
        }

        Console.WriteLine($"{new string('-', depth * 2)}{node.Data}");

        foreach (var child in node.Children)
        {
            PrintTreeRecursive(child, depth + 1);
        }
    }
}