﻿
namespace Airlines.Business.CustomExceptions;
public class CommandSplitException : Exception
{
    public CommandSplitException() { }

    public CommandSplitException(string message)
        : base(message) { }

    public CommandSplitException(string message, Exception innerException)
        : base(message, innerException) { }
}