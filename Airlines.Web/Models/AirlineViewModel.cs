﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Airlines.Web.Models;

public class AirlineViewModel
{
    [Key]
    [Column("ID")]
    public int Id { get; set; }

    [StringLength(256)]
    [Unicode(false)]
    public string? Name { get; set; }

    public DateOnly? Founded { get; set; }

    public int? FleetSize { get; set; }

    [StringLength(2048)]
    public string? Description { get; set; }
}
