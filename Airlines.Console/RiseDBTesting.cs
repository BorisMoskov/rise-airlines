﻿using Airlines.Persistence.Repositories;
using Airlines.Persistance.Models;
using Airlines.Persistence.Repositories.Interfaces;

namespace Airlines.Console;
internal class RiseDBTesting
{
    /*private readonly FlightRepository _flightRepository;
    private readonly AirlineRepository _airlineRepository;
    private readonly AirportRepository _airportRepository;

    public RiseDBTesting()
    {
        _flightRepository = new FlightRepository();
        _airlineRepository = new AirlineRepository();
        _airportRepository = new AirportRepository();
    }

    public void ListFlights()
    {
        System.Console.WriteLine("---------------FLIGHTS---------------");
        var flightList = _flightRepository.GetFlights();
        foreach (var flight in flightList)
        {
            System.Console.WriteLine($"Flight Number: {flight.FlightNumber}, Departure airport id: {flight.DepartureAirportId}, Arrival airport id: {flight.ArrivalAirportId}, Departure Time: {flight.DepartureDateTime}, Arrival Time: {flight.ArrivalDateTime}");
        }
        System.Console.WriteLine();
    }
    public void ListAirlines()
    {
        System.Console.WriteLine("---------------AIRLINES---------------");
        var airlineList = _airlineRepository.GetAirlines();
        foreach (var airline in airlineList)
        {
            System.Console.WriteLine($"Airline name: {airline.Name}, Airline founded in: {airline.Founded}, Airline fleet size: {airline.FleetSize}, Airline descritpion: {airline.Description}");
        }
        System.Console.WriteLine();
    }
    public void ListAirports()
    {
        System.Console.WriteLine("---------------AIRPORTS---------------");
        var airportList = _airportRepository.GetAirports();
        foreach (var airport in airportList)
        {
            System.Console.WriteLine($"Airport name: {airport.Name}, Airport country: {airport.Country}, Airport city: {airport.City}, Airport code: {airport.Code}, Airport runways count: {airport.RunwaysCount}, Airport founded in: {airport.Founded}");
        }
        System.Console.WriteLine();
    }
    public List<Flight> ListFlights(Func<Flight, bool> filter)
    {
        using var flightRepository = new FlightRepository();
        return flightRepository.GetFlights(filter);
    }
    public List<Airline> ListAirlines(Func<Airline, bool> filter)
    {
        using var airlineRepository = new AirlineRepository();
        return airlineRepository.GetAirlines(filter);
    }
    public List<Airport> ListAirports(Func<Airport, bool> filter)
    {
        using var airportRepository = new AirportRepository();
        return airportRepository.GetAirports(filter);
    }

    public void AddFlight(Flight newFlight)
    {
        using var flightRepository = new FlightRepository();
        bool addSuccess = flightRepository.AddFlight(newFlight);
        if (addSuccess)
        {
            System.Console.WriteLine("New flight added successfully.");
        }
        else
        {
            System.Console.WriteLine("Failed to add new flight.");
        }
    }

    public void AddAirline(Airline newAirline)
    {
        using var airlineRepository = new AirlineRepository();
        bool addSuccess = airlineRepository.AddAirline(newAirline);

        if (addSuccess)
        {
            System.Console.WriteLine("Airline added successfully.");
        }
        else
        {
            System.Console.WriteLine("Failed to add airline.");
        }
    }

    public void AddAirport(Airport newAirport)
    {
        using var airportRepository = new AirportRepository();
        bool addSuccess = airportRepository.AddAirport(newAirport);
        if (addSuccess)
        {
            System.Console.WriteLine("New airport added successfully.");
        }
        else
        {
            System.Console.WriteLine("Failed to add new airport.");
        }
    }

    public void UpdateFlight(Flight flightToUpdate)
    {
        using var flightRepository = new FlightRepository();
        bool updateSuccess = flightRepository.UpdateFlight(flightToUpdate);
        if (updateSuccess)
        {
            System.Console.WriteLine("Flight updated successfully.");
        }
        else
        {
            System.Console.WriteLine("Failed to update flight.");
        }
    }

    public void UpdateAirline(Airline airline)
    {
        using var airlineRepository = new AirlineRepository();
        bool updateSuccess = airlineRepository.UpdateAirline(airline);

        if (updateSuccess)
        {
            System.Console.WriteLine("Airline updated successfully.");
        }
        else
        {
            System.Console.WriteLine("Failed to update airline.");
        }
    }

    public void UpdateAirport(Airport airportToUpdate)
    {
        using var airportRepository = new AirportRepository();
        bool updateSuccess = airportRepository.UpdateAirport(airportToUpdate);
        if (updateSuccess)
        {
            System.Console.WriteLine("Airport updated successfully.");
        }
        else
        {
            System.Console.WriteLine("Failed to update airport.");
        }
    }

    public void DeleteFlight(Flight flight)
    {
        using var flightRepository = new FlightRepository();
        bool deleteSuccess = flightRepository.DeleteFlight(flight.Id);
        if (deleteSuccess)
        {
            System.Console.WriteLine("Flight deleted successfully.");
        }
        else
        {
            System.Console.WriteLine("Failed to delete flight.");
        }
    }

    public void DeleteAirline(Airline airline)
    {
        using var airlineRepository = new AirlineRepository();
        bool deleteSuccess = airlineRepository.DeleteAirline(airline.Id);

        if (deleteSuccess)
        {
            System.Console.WriteLine("Airline deleted successfully.");
        }
        else
        {
            System.Console.WriteLine("Failed to delete airline.");
        }
    }

    public void DeleteAirport(Airport airport)
    {
        using var airportRepository = new AirportRepository();
        bool deleteSuccess = airportRepository.DeleteAirport(airport.Id);
        if (deleteSuccess)
        {
            System.Console.WriteLine("Airport deleted successfully.");
        }
        else
        {
            System.Console.WriteLine("Failed to delete airport.");
        }
    }*/
}
