import { fetchAirlines, createAirline, deleteAirline, updateAirline } from './httpRequests.js';

document.addEventListener('DOMContentLoaded', () => {
    initialize();
});

function initialize() {
    loadAirlines();
    setupEventListeners();
}

async function loadAirlines() {
    try {
        const airlines = await fetchAirlines();
        populateAirlinesTable(airlines);
    } catch (error) {
        console.error('Failed to load airlines:', error);
    }
}

function populateAirlinesTable(airlines) {
    const tableBody = document.getElementById('airlinesTableBody');
    tableBody.innerHTML = '';

    airlines.forEach((airline, index) => {
        const row = document.createElement('tr');
        row.innerHTML = `
            <td>${airline.name}</td>
            <td>${airline.founded}</td>
            <td>${airline.fleetSize}</td>
            <td>${airline.description}</td>
            <td>
                <button class="edit-button" data-id="${airline.id}">Edit</button>
                <button class="delete-button" data-id="${airline.id}">Delete</button>
            </td>
        `;
        if (index >= 3) {
            row.style.display = 'none';
        }
        tableBody.appendChild(row);
    });

    document.querySelectorAll('.delete-button').forEach(button => {
        button.addEventListener('click', async (event) => {
            const id = event.target.getAttribute('data-id');
            try {
                await deleteAirline(id);
                loadAirlines();
            } catch (error) {
                console.error('Failed to delete airline:', error);
            }
        });
    });

    document.querySelectorAll('.edit-button').forEach(button => {
        button.addEventListener('click', (event) => {
            const id = event.target.getAttribute('data-id');
            editAirline(id);
        });
    });

    const airlineForm = document.getElementById('airlineForm');
    if (airlineForm) {
        airlineForm.addEventListener('submit', async function(event) {
            event.preventDefault();
            
            try {
                const name = document.getElementById('name').value;
                const founded = document.getElementById('founded').value;
                const fleetSize = document.getElementById('fleet_size').value;
                const description = document.getElementById('description').value;
                
                const newAirline = { name, founded, fleetSize, description };

                await createAirline(newAirline);
                location.reload();
            } catch (error) {
                console.error('Failed to create airline:', error);
                location.reload();
            }
        });
    }


    const editAirlineForm = document.getElementById('editAirlineForm');
    editAirlineForm.addEventListener('submit', async function(event) {
        event.preventDefault();

        const id = document.getElementById('editId').value;
        const name = document.getElementById('editName').value;
        const founded = document.getElementById('editFounded').value;
        const fleetSize = document.getElementById('editFleetSize').value;
        const description = document.getElementById('editDescription').value;

        const updatedAirline = { id, name, founded, fleetSize, description };

        try {
            await updateAirline(updatedAirline);
            location.reload();
            editAirlineForm.style.display = 'none';
        } catch (error) {
            console.error('Failed to update airline:', error);
            location.reload();
        }
    });

    const deleteButton = document.getElementById('deleteButton');
    deleteButton.addEventListener('click', async function() {
        const deleteAirlineId = document.getElementById('deleteAirlineId').value;
        if (deleteAirlineId) {
            try {
                await deleteAirline(deleteAirlineId);
                loadAirlines();
            } catch (error) {
                console.error('Failed to delete airline:', error);
            }
        } else {
            console.error('Please enter a valid Airline ID.');
        }
    });
}

function editAirline(id) {
    const airlineForm = document.getElementById('airlineForm');
    const editAirlineForm = document.getElementById('editAirlineForm');

    airlineForm.style.display = 'none';
    editAirlineForm.style.display = 'block';

    const airline = Array.from(document.querySelectorAll(`#airlinesTableBody tr`)).find(row => row.querySelector('.edit-button').getAttribute('data-id') === id);

    document.getElementById('editId').value = id;
    document.getElementById('editName').value = airline.cells[0].textContent;
    document.getElementById('editFounded').value = airline.cells[1].textContent;
    document.getElementById('editFleetSize').value = airline.cells[2].textContent;
    document.getElementById('editDescription').value = airline.cells[3].textContent;
}
