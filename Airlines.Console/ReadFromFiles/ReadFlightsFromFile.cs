﻿using Airlines.Business.Models;
using System.Globalization;

namespace Airlines.Console.ReadFromFiles;
internal class ReadFlightsFromFile
{
    internal static LinkedList<Flight> ReadFromFile()
    {
        var filePath = "../../../InputData/FlightsData.csv";
        var flights = new LinkedList<Flight>();

        using (var reader = new StreamReader(filePath))
        {
            string line;
            while ((line = reader.ReadLine()!) != null)
            {
                var parts = line.Split(',');
                if (parts.Length == 6)
                {
                    var identifier = parts[0];
                    var departureAirport = parts[1];
                    var arrivalAirport = parts[2];
                    var aircraftModel = parts[3];
                    var price = double.Parse(parts[4], CultureInfo.InvariantCulture);
                    var time = double.Parse(parts[5], CultureInfo.InvariantCulture);

                    var flight = new Flight(identifier, departureAirport, arrivalAirport, aircraftModel, price, time);
                    _ = flights.AddLast(flight);
                }
            }
        }

        return flights;
    }
}
