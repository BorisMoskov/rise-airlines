﻿document.addEventListener('DOMContentLoaded', function () {
    console.log('DOMContentLoaded event fired');

    const toggleFormButton = document.getElementById('toggleButton');
    const showMoreButton = document.getElementById('showMore');
    const showSearchBarButton = document.getElementById('showSearchBar');
    const form = document.querySelector('.form');
    const table = document.querySelector('.table');
    const tableRows = table.querySelectorAll('tbody tr');

    form.style.display = 'none';

    toggleFormButton.addEventListener('click', function () {
        console.log('Toggle form button clicked');

        if (form.style.display === 'none') {
            form.style.display = 'flex';
            table.style.display = 'none';
            toggleFormButton.textContent = 'Show Table';
            showMoreButton.style.display = 'none';
            showSearchBarButton.style.display = 'none';
        } else {
            form.style.display = 'none';
            table.style.display = 'flex';
            toggleFormButton.textContent = 'Show Form';
            showMoreButton.style.display = 'block';
            showSearchBarButton.style.display = 'block';
        }
    });

    let showAll = false;
    let initialRowsDisplayed = 3;

    showMoreButton.addEventListener('click', function () {
        console.log('Show more button clicked');

        if (!showAll) {
            for (let i = 0; i < tableRows.length; i++) {
                tableRows[i].style.display = 'table-row';
            }
            showAll = true;
            showMoreButton.textContent = 'Show Less';
        } else {
            for (let i = 0; i < tableRows.length; i++) {
                if (i < initialRowsDisplayed) {
                    tableRows[i].style.display = 'table-row';
                } else {
                    tableRows[i].style.display = 'none';
                }
            }
            showAll = false;
            showMoreButton.textContent = 'Show More';
        }
    });

    for (let i = initialRowsDisplayed; i < tableRows.length; i++) {
        tableRows[i].style.display = 'none';
    }
});
