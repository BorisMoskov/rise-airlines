document.addEventListener('DOMContentLoaded', function () {
    const form = document.getElementById('airportForm');
    const submitButton = document.getElementById('submitButton');
    const errorMessages = document.getElementById('errorMessages');

    submitButton.disabled = true;

    const fields = [
        { id: 'name', error: 'Name must be at most 100 characters long', validate: (value) => value.trim() === '' || value.length > 100 },
        { id: 'country', error: 'Country must be at most 30 symbols', validate: (value) => value.length > 30 },
        { id: 'city', error: 'City must be at most 30 symbols', validate: (value) => value.length > 30 },
        { id: 'code', error: 'Code must be at most 3 symbols', validate: (value) => value.length > 3 },
        { id: 'runways_count', error: 'Runways count must be at least 1', validate: (value) => value < 1 },
        { id: 'founded', error: "Founded date must be less than or equal to today's date", validate: (value) => new Date(value) > new Date() || isNaN(new Date(value)) }
    ];

    function validateField(field) {
        const fieldElement = document.getElementById(field.id);
        const errorMessage = fieldElement.nextElementSibling;

        const isValid = !field.validate || !field.validate(fieldElement.value);
        errorMessage.textContent = isValid ? '' : field.error;

        return isValid;
    }

    function validateForm() {
        const isFormValid = fields.every(validateField);
        submitButton.disabled = !isFormValid;
        errorMessages.textContent = isFormValid ? '' : 'Please fill in all required fields.';
    }

    function addValidationListeners() {
        fields.forEach(field => {
            const fieldElement = document.getElementById(field.id);

            fieldElement.addEventListener('blur', validateForm);
            fieldElement.addEventListener('input', () => {
                const errorMessage = fieldElement.nextElementSibling;
                errorMessage.textContent = '';
                validateForm();
            });
        });
    }

    addValidationListeners();

    form.addEventListener('submit', function (event) {
        const isFormValid = fields.every(validateField);
        if (!isFormValid) {
            event.preventDefault();
            submitButton.disabled = true;
            errorMessages.textContent = 'Please fill in all required fields.';
        } else {
            errorMessages.textContent = '';
        }
    });
});
