﻿using Airlines.Business;
using Airlines.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.UnitTests;
public class RouteManagementTests
{
    [Fact]
    public void CheckIfFlightExist_ReturnsFlightIfExists()
    {
        var flights = new LinkedList<Flight>();
        _ = flights.AddLast(new Flight("FL123", "JFK", "LAX", "Boeing 747", 350, 6));
        _ = flights.AddLast(new Flight("FL456", "LAX", "ORD", "Boeing 747", 280, 4));
        _ = flights.AddLast(new Flight("FL789", "ORD", "ATL", "Boeing 747", 120, 2));
        var flight = new Flight("FL123", "JFK", "LAX", "Boeing 747", 90, 1);

        var result = RouteManagement.CheckIfFlightExist(flights, "FL123");

        Assert.Equal(flight.Identifier, result!.Identifier);
        Assert.Equal(flight.DepartureAirport, result.DepartureAirport);
        Assert.Equal(flight.ArrivalAirport, result.ArrivalAirport);
    }

    [Fact]
    public void CheckIfFlightExist_ReturnsNullIfFlightDoesNotExist()
    {
        var flights = new LinkedList<Flight>();

        var result = RouteManagement.CheckIfFlightExist(flights, "ABC123");

        Assert.Null(result);
    }

    [Fact]
    public void AddFlight_ValidCommandWithExistingFlight_AddsFlightToRoute()
    {
        var routeManagement = new RouteManagement();
        var flights = new LinkedList<Flight>();
        var existingFlight = new Flight("FL123", "JFK", "LAX", "Boeing 747", 350, 6);
        _ = flights.AddLast(existingFlight);

        routeManagement.AddFlight("route add FL123", flights);

        //Assert.Contains(existingFlight, routeManagement.GetRoute());
    }

    [Fact]
    public void PrintRouteTest()
    {
        var routeManagement = new RouteManagement();
        var flights = new LinkedList<Flight>();
        _ = flights.AddLast(new Flight("FL123", "JFK", "LAX", "Boeing 747", 350, 6));
        _ = flights.AddLast(new Flight("FL456", "LAX", "ORD", "Boeing 747", 240, 4));
        _ = flights.AddLast(new Flight("FL789", "ORD", "ATL", "Boeing 747", 120, 3));
        _ = flights.AddLast(new Flight("FL101", "DCA", "SFO", "Boeing 747", 80, 1));
        routeManagement.AddFlight("route add FL123", flights);

        using StringWriter sw = new StringWriter();

        routeManagement.PrintRoute();

        string expectedOutput = $"{Environment.NewLine}Route:{Environment.NewLine}" +
                                $"Flight FL123: JFK -> LAX{Environment.NewLine}" +
                                $"Flight FL456: LAX -> ORD{Environment.NewLine}" +
                                $"Flight FL789: ORD -> ATL{Environment.NewLine}" +
                                $"Flight FL101: DCA -> SFO{Environment.NewLine}" +
                                $"{Environment.NewLine}";
        //Assert.Equal(expectedOutput, sw.ToString());
    }

    [Fact]
    public void RemoveLastFlight_RouteNotEmpty_RemovesLastFlight()
    {
        var routeManagement = new RouteManagement();
        var flight1 = new Flight("FL123", "JFK", "LAX", "Boeing 747", 350, 6);
        var flight2 = new Flight("FL456", "LAX", "ORD", "Airbus A320", 95, 2);
        _ = routeManagement._route.AddLast(flight1);
        _ = routeManagement._route.AddLast(flight2);

        routeManagement.RemoveLastFlight();

        Assert.DoesNotContain(flight2, routeManagement._route);
    }
}
