const API_BASE_URL = 'https://localhost:7117/api/Flight';

async function fetchFlights() {
    try {
        const response = await fetch(API_BASE_URL);
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        return await response.json();
    } catch (error) {
        console.error('Error fetching flights:', error);
        throw error;
    }
}

async function createFlight(newFlight) {
    try {
        const response = await fetch(API_BASE_URL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(newFlight)
        });
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        return await response.json();
    } catch (error) {
        console.error('Error creating flight:', error);
        throw error;
    }
}

async function deleteFlight(id) {
    try {
        const response = await fetch(`${API_BASE_URL}/${id}`, {
            method: 'DELETE'
        });
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
    } catch (error) {
        console.error('Error deleting flight:', error);
        throw error;
    }
}

async function updateFlight(updatedFlight) {
    try {
        const response = await fetch(`${API_BASE_URL}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(updatedFlight)
        });
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
    } catch (error) {
        console.error('Error updating flight:', error);
        throw error;
    }
}

export { fetchFlights, createFlight, deleteFlight, updateFlight };
