﻿using Airlines.Persistance.Models;
using Microsoft.AspNetCore.Mvc;
using Airlines.Business.Services;

namespace Airlines.Web.Controllers;
public class AirlineController : Controller
{
    private readonly AirlineService _airlineService;
    public AirlineController(AirlineService airlineService)
    {
        _airlineService = airlineService;
    }
    public IActionResult Index()
    {
        List<Airline> airlineList = _airlineService.GetAirlines();
        return View(airlineList);
    }

    [HttpPost]
    public ActionResult CreateNewAirline(Airline airline)
    {
        if (airline.Founded.HasValue && airline.Founded.Value > DateOnly.FromDateTime(DateTime.Today))
        {
            ModelState.AddModelError("Founded", "Invalid date.");
        }
        if (ModelState.IsValid)
        {
            _airlineService.AddAirlineToDB(airline);
            TempData["SuccessMessage"] = "Airline added successfully";
            return RedirectToAction(nameof(Index));
        }
        TempData["ErrorMessage"] = "The Airline was not added";
        List<Airline> airlineList = _airlineService.GetAirlines();
        return View("Index", airlineList);
    }

    public IActionResult Search(string searchTerm)
    {
        List<Airline> airlines = _airlineService.SearchInAirline(searchTerm);
        return View("Index", airlines);
    }
}
