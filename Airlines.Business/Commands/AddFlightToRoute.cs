﻿using Airlines.Business.Models;
using System.Diagnostics.CodeAnalysis;

namespace Airlines.Business.Commands;
internal class AddFlightToRoute(string command, LinkedList<Flight> flights) : ICommand
{
    private readonly RouteManagement _manager = new();
    private readonly LinkedList<Flight> _flights = flights;

    public void Execute() => _manager.AddFlight(command, _flights);
}