﻿using Airlines.Business.Models;
using AirlinesBusiness;
using System.Diagnostics.CodeAnalysis;

namespace Airlines.Business.Commands;
[ExcludeFromCodeCoverage]
public class ListAllAirportsCityCountry(Dictionary<string, List<Airport>> airportsByCity, Dictionary<string, List<Airport>> airportsByCountry, string command) : ICommand
{
    public void Execute()
    {
        string[] commandParts;
        string inputData;
        string from;
        List<Airport> matchingAirports = [];

        try
        {
            commandParts = command.Split(' ');
            if (commandParts.Length == 3)
            {
                inputData = commandParts[1];
                from = commandParts[^1].ToLower();
            }
            else
            {
                inputData = commandParts[1] + " " + commandParts[2];
                from = commandParts[^1].ToLower();
            }
        }
        catch
        {
            Console.WriteLine("Incorrect command format");
            Console.WriteLine("list <input data> <from>");
            return;
        }

        switch (from)
        {
            case "city":
                // Check if the city exists in the dictionary
                if (airportsByCity.TryGetValue(inputData, out var value))
                {
                    matchingAirports.AddRange(value);
                }
                break;
            case "country":
                // Check if the country exists in the dictionary
                if (airportsByCountry.TryGetValue(inputData, out value))
                {
                    matchingAirports.AddRange(value);
                }
                break;
            default:
                Console.WriteLine("Not found");
                break;
        }

        PrintInformation.PrintAirportsInCityCountry(matchingAirports);
    }
}
