import { fetchAirports, createAirport, deleteAirport, updateAirport } from './httpRequests.js';

document.addEventListener('DOMContentLoaded', () => {
    initialize();
});

function initialize() {
    loadAirports();
    setupEventListeners();
}

async function loadAirports() {
    try {
        const airports = await fetchAirports();
        populateAirportsTable(airports);
    } catch (error) {
        console.error('Failed to load airports:', error);
    }
}

function populateAirportsTable(airports) {
    const tableBody = document.getElementById('airportsTableBody');
    tableBody.innerHTML = '';

    airports.forEach((airport, index) => {
        const row = document.createElement('tr');
        row.innerHTML = `
            <td>${airport.name}</td>
            <td>${airport.country}</td>
            <td>${airport.city}</td>
            <td>${airport.code}</td>
            <td>${airport.runwaysCount}</td>
            <td>${airport.founded}</td>
            <td>
                <button class="edit-button" data-id="${airport.id}">Edit</button>
                <button class="delete-button" data-id="${airport.id}">Delete</button>
            </td>
        `;
        if (index >= 3) {
            row.style.display = 'none';
        }
        tableBody.appendChild(row);
    });

    document.querySelectorAll('.delete-button').forEach(button => {
        button.addEventListener('click', async (event) => {
            const id = event.target.getAttribute('data-id');
            try {
                await deleteAirport(id);
                location.reload();;
            } catch (error) {
                console.error('Failed to delete airport:', error);
                location.reload();
            }
        });
    });

    document.querySelectorAll('.edit-button').forEach(button => {
        button.addEventListener('click', (event) => {
            const id = event.target.getAttribute('data-id');
            editAirport(id);
        });
    });

    const airportForm = document.getElementById('airportForm');
    if (airportForm) {
        airportForm.addEventListener('submit', async function(event) {
            event.preventDefault();
            
            try {
                const name = document.getElementById('name').value;
                const country = document.getElementById('country').value;
                const city = document.getElementById('city').value;
                const code = document.getElementById('code').value;
                const runwaysCount = document.getElementById('runways_count').value;
                const founded = document.getElementById('founded').value;
                
                const newAirport = { name, country, city, code, runwaysCount, founded };

                await createAirport(newAirport);
                location.reload();
            } catch (error) {
                console.error('Failed to create airport:', error);
                location.reload();
            }
        });
    }


    const editAirportForm = document.getElementById('editAirportForm');
    editAirportForm.addEventListener('submit', async function(event) {
        event.preventDefault();

        const id = document.getElementById('editId').value;
        const name = document.getElementById('editName').value;
        const country = document.getElementById('editCountry').value;
        const city = document.getElementById('editCity').value;
        const code = document.getElementById('editCode').value;
        const runwaysCount = document.getElementById('editRunwaysCount').value;
        const founded = document.getElementById('editFounded').value;

        const updatedAirport = { id, name, country, city, code, runwaysCount, founded };

        try {
            await updateAirport(updatedAirport);
            location.reload();
            editAirportForm.style.display = 'none';
        } catch (error) {
            console.error('Failed to update airport:', error);
            location.reload();
        }
    });
}

function editAirport(id) {
    const airportForm = document.getElementById('airportForm');
    const editAirportForm = document.getElementById('editAirportForm');

    airportForm.style.display = 'none';
    editAirportForm.style.display = 'block';

    const airport = Array.from(document.querySelectorAll(`#airportsTableBody tr`)).find(row => row.querySelector('.edit-button').getAttribute('data-id') === id);

    document.getElementById('editId').value = id;
    document.getElementById('editName').value = airport.cells[0].textContent;
    document.getElementById('editCountry').value = airport.cells[1].textContent;
    document.getElementById('editCity').value = airport.cells[2].textContent;
    document.getElementById('editCode').value = airport.cells[3].textContent;
    document.getElementById('editRunwaysCount').value = airport.cells[4].textContent;
    document.getElementById('editFounded').value = airport.cells[5].textContent;
}
