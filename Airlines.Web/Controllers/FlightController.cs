﻿using Airlines.Persistance.Models;
using Microsoft.AspNetCore.Mvc;
using Airlines.Business.Services;

namespace Airlines.Web.Controllers;
public class FlightController : Controller
{
    private readonly FlightService _flightService;
    public FlightController(FlightService flightService)
    {
        _flightService = flightService;
    }
    public IActionResult Index()
    {
        List<Flight> flightList = _flightService.GetFlights();
        return View(flightList);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult AddFlight(Flight flight)
    {
        if (flight.DepartureDateTime.HasValue && flight.DepartureDateTime.Value < DateTime.Now)
        {
            ModelState.AddModelError("DepartureDateTime", "Departure time must be in the future.");
        }

        if (flight.ArrivalDateTime.HasValue && flight.ArrivalDateTime.Value <= flight.DepartureDateTime)
        {
            ModelState.AddModelError("ArrivalDateTime", "Arrival time must be after the departure time.");
        }

        if (ModelState.IsValid)
        {
            _flightService.AddFlightToDB(flight);
            TempData["SuccessMessage"] = "Flight added successfully";
            return RedirectToAction(nameof(Index));
        }
        TempData["ErrorMessage"] = "The Flight was not added";
        List<Flight> flightList = _flightService.GetFlights();
        return View("Index", flightList);
    }

    public IActionResult Search(string searchTerm)
    {
        List<Flight> flight = _flightService.SearchInFlight(searchTerm);
        return View("Index", flight);
    }
}
