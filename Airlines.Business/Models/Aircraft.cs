﻿namespace Airlines.Business.Models;
public class Aircraft(string model)
{
    public string Model { get; set; } = model;
}

public class CargoAircraft(string model, double cargoWeight, double cargoVolume) : Aircraft(model)
{
    public double CargoWeight { get; set; } = cargoWeight;
    public double CargoVolume { get; set; } = cargoVolume;
}

public class PassengerAircraft(string model, double cargoWeight, double cargoVolume, int seats) : Aircraft(model)
{
    public int Seats { get; set; } = seats;
    public double CargoWeight { get; set; } = cargoWeight;
    public double CargoVolume { get; set; } = cargoVolume;
}

public class PrivateAircraft(string model, int seats) : Aircraft(model)
{
    public int Seats { get; set; } = seats;
}