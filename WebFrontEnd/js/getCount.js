const API_ENDPOINTS = {
    AIRPORTS: 'https://localhost:7117/api/Airport',
    AIRLINES: 'https://localhost:7117/api/Airline',
    FLIGHTS: 'https://localhost:7117/api/Flight'
};

async function fetchTableCount(endpoint) {
    try {
        const response = await fetch(endpoint);
        if (!response.ok) {
            throw new Error(`Failed to fetch data from ${endpoint}`);
        }
        const data = await response.json();
        return data.length;
    } catch (error) {
        console.error(`Error fetching data from ${endpoint}:`, error);
        return 0;
    }
}

async function updateTableCount() {
    const airportCount = await fetchTableCount(API_ENDPOINTS.AIRPORTS);
    const airlineCount = await fetchTableCount(API_ENDPOINTS.AIRLINES);
    const flightCount = await fetchTableCount(API_ENDPOINTS.FLIGHTS);

    document.getElementById('airportCount').textContent = airportCount;
    document.getElementById('airlineCount').textContent = airlineCount;
    document.getElementById('flightCount').textContent = flightCount;
}

updateTableCount();
