﻿namespace Airlines.Business.Models;
public class Airline
{
    public static bool IsValidName(string parameter)
    {
        foreach (var symbol in parameter)
            if (!char.IsLetter(symbol) && symbol != ' ' && symbol != '.')
                return false;
        return true;
    }
}