﻿using Airlines.Business.Models;

namespace Airlines.Console.ReadFromFiles;
public class ReadAircraftsFromFile
{
    internal static void LoadAircraftDefinitions(Dictionary<string, CargoAircraft> cargoAircraftDictionary, Dictionary<string, PassengerAircraft> passengerAircraftDictionary, Dictionary<string, PrivateAircraft> privateAircraftDictionary)
    {
        var filePath = "../../../InputData/AircraftsData.csv";
        using var reader = new StreamReader(filePath);
        string line;
        while ((line = reader.ReadLine()!) != null)
        {
            var fields = line.Split(',');

            var model = fields[0];
            var cargoWeight = ParseDouble(fields[1]);
            var cargoVolume = ParseDouble(fields[2]);
            var seats = ParseInt(fields[3]);

            if (cargoWeight != -1 && cargoVolume != -1 && seats != -1)
                // Passenger Aircraft
                passengerAircraftDictionary.Add(model, new PassengerAircraft(model, cargoWeight, cargoVolume, seats));
            else if (cargoWeight != -1 && cargoVolume != -1)
                /// Cargo Aircraft
                cargoAircraftDictionary.Add(model, new CargoAircraft(model, cargoWeight, cargoVolume));
            else
                // Private Aircraft
                privateAircraftDictionary.Add(model, new PrivateAircraft(model, seats));
        }
    }

    private static double ParseDouble(string value)
    {
        if (double.TryParse(value, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out var result))
            return result;
        return -1;
    }

    private static int ParseInt(string value) => int.TryParse(value, out var result) ? result : -1;
}