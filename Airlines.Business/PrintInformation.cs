﻿
using Airlines.Business.Models;
using System.Diagnostics.CodeAnalysis;
using System.IO;

namespace AirlinesBusiness;
[ExcludeFromCodeCoverage]
public class PrintInformation
{
    public static void PrintAirports(Dictionary<string, Airport> airports)
    {
        Console.WriteLine("Airports: ");
        foreach (var item in airports)
        {
            Console.WriteLine($"{item.Value.Identifier} {item.Value.Name} {item.Value.City} {item.Value.Country}");
        }
        //New line for readability
        Console.WriteLine();
    }

    public static void PrintAirlines(HashSet<string> airlines)
    {
        // Print data from airline file
        Console.WriteLine("Airlines:");
        foreach (var airline in airlines)
        {
            Console.WriteLine(airline);
        }
        //New line for readability
        Console.WriteLine();
    }

    public static void PrintFlights(LinkedList<Flight> flights)
    {
        Console.WriteLine("Flights:");
        foreach (var flight in flights)
        {
            Console.WriteLine($"{flight.Identifier} {flight.DepartureAirport} {flight.ArrivalAirport} {flight.AircraftModel}");
        }
        //New line for readability
        Console.WriteLine();
    }

    public static void PrintMenu()
    {
        string normal = Console.IsOutputRedirected ? "" : "\x1b[39m";
        string red = Console.IsOutputRedirected ? "" : "\x1b[91m";
        string yellow = Console.IsOutputRedirected ? "" : "\x1b[93m";

        Console.WriteLine("Command option: ");
        Console.WriteLine($"Search command: {yellow}search <search term>{normal}");
        Console.WriteLine("Searches for the word in all data lists");
        Console.WriteLine($"Sort command: sort: {yellow}sort <input data> [order]{normal}");
        Console.WriteLine("Sorts a given collection (Airports,Airlines,Flights)");
        Console.WriteLine($"Exist command: {yellow}exist <airline name>{normal}");
        Console.WriteLine("Checks if an airline exists");
        Console.WriteLine($"List command: {yellow}list <input data> <from>{normal}");
        Console.WriteLine("List all airports in a city or a country");
        Console.WriteLine($"Create New Route command: {yellow}route new{normal}");
        Console.WriteLine($"Add Flight to Route command: {yellow}route add <Flight Identifier>{normal}");
        Console.WriteLine($"Remove Flight at End command: {yellow}route remove{normal}");
        Console.WriteLine($"Print route command: {yellow}route print{normal}");
        Console.WriteLine($"Cargo reservation command: {yellow}reserve cargo{normal}");
        Console.WriteLine($"Passenger ticket reservation command: {yellow}reserve ticket{normal}");
        Console.WriteLine($"Print route from  specific airport (DFW) to anywhere: {yellow}route find <Destination Airport>{normal}");
        Console.WriteLine($"Determine if two airports are connected and find a route: {yellow}route search <Start Airport> <End Airport> <Strategy>{normal}");
        Console.WriteLine($"<Strategy> options: {yellow}cheap -> least price, time -> shortest time, stops -> fewest stops{normal}");
        Console.WriteLine($"Enter {red}done{normal} when you are finished");
    }

    public static void PrintSearchSpecificWordType(string type, string targetWord)
    {
        switch (type)
        {
            case "airports":
                Console.WriteLine($"{targetWord} is an Airport.");
                break;
            case "airlines":
                Console.WriteLine($"{targetWord} is an Airline.");
                break;
            case "flights":
                Console.WriteLine($"{targetWord} is a Flight.");
                break;
            default:
                Console.WriteLine($"{targetWord} not found.");
                break;
        }

        //New line for readability
        Console.WriteLine();
    }

    public static void PrintIfAirlineExist(bool isExist, string airlineName)
    {
        if (isExist)
        {
            Console.WriteLine($"Airline {airlineName} exists");
        }
        else
        {
            Console.WriteLine($"Airline {airlineName} does not exists");
        }
        Console.WriteLine();
    }

    public static void PrintAirportsInCityCountry(List<Airport> matchingAirports)
    {
        foreach (var airport in matchingAirports)
        {
            Console.WriteLine($"{airport.Identifier} {airport.Name}");
        }
    }

    public static void PrintReservationInfo(string flightIdentifier) => Console.WriteLine($"Booked flgiht {flightIdentifier} successfully");

    public static void PrintRoutePath(bool isConnected, List<string> path)
    {
        if (isConnected)
        {
            Console.WriteLine("The airports are connected.");
            Console.WriteLine("Path:");
            for (int i = 0; i < path.Count; i++)
            {
                Console.WriteLine($"{i + 1}: {path[i]}");
            }
            Console.WriteLine();
        }
        else
        {
            Console.WriteLine("The airports are not connected.");
            Console.WriteLine();
        }
    }

    public static void PrintRoutePathByPrice(bool isConnected, List<string> path, Dictionary<string, double> prices)
    {
        if (isConnected)
        {
            Console.WriteLine("The airports are connected.");
            Console.WriteLine("Path:");
            double totalPrice = 0;
            for (int i = 0; i < path.Count; i++)
            {
                string airport = path[i];
                double price = prices[airport];
                totalPrice += price;
                Console.WriteLine($"{i + 1}: {airport} - Price: {price}");
            }
            Console.WriteLine($"Total price: {totalPrice}lv.");
            Console.WriteLine();
        }
        else
        {
            Console.WriteLine("The airports are not connected.");
            Console.WriteLine();
        }
    }

    public static void PrintRoutePathByTime(bool isConnected, List<string> path, Dictionary<string, double> times)
    {
        if (isConnected)
        {
            double totalTime = 0;
            Console.WriteLine("The airports are connected.");
            Console.WriteLine("Path:");
            for (int i = 0; i < path.Count; i++)
            {
                Console.WriteLine($"{i + 1}: {path[i]}");
                totalTime += times[path[i]];
            }
            Console.WriteLine($"Total time: {totalTime} hours");
        }
        else
        {
            Console.WriteLine("The airports are not connected.");
        }
    }
}
