﻿using Airlines.Business.Models;
using Airlines.Business.Commands;
using AirlinesBusiness;
using Airlines.Business.CustomExceptions;
using System.Globalization;

namespace Airlines.Business
{
    public static class CommandFactory
    {
        internal static ICommand? CreateCommand(Dictionary<string, Airport> airports, Dictionary<string, List<Airport>> airportsByCity, Dictionary<string,
            List<Airport>> airportsByCountry, HashSet<string> airlines, LinkedList<Flight> flights, RouteManagement manager,
            Dictionary<string, CargoAircraft> cargoAircraftDictionary,
            Dictionary<string, PassengerAircraft> passengerAircraftDictionary, FlightRouteTree routeTree,
            Graph<string> flightGraph, string fullCommand)
        {
            string[] parts;
            string command;
            string parameters;

            try
            {
                // Split the full command into the command and its parameters
                parts = fullCommand.Split(' ');
                command = parts[0].ToLower();
                parameters = fullCommand[command.Length..].Trim();
            }
            catch (Exception ex)
            {
                throw new CommandSplitException("An error occurred while splitting the command.", ex);
            }

            switch (command)
            {
                case "search":
                    return new SearchSpecificData(airports, airlines, flights, fullCommand);

                case "sort":
                    return new SortSpecificData(airports, airlines, flights, fullCommand);

                case "exist":
                    return new ExistAirline(airlines, fullCommand);

                case "list":
                    return new ListAllAirportsCityCountry(airportsByCity, airportsByCountry, fullCommand);

                case "route":
                    if (parameters == "new")
                    {
                        return new NewRoute();
                    }
                    else if (parameters == "remove")
                    {
                        manager.RemoveLastFlight();
                        break;
                    }
                    else if (parameters.Split(" ")[0] == "add")
                    {
                        manager.AddFlight(fullCommand, flights);
                        break;
                    }
                    else if (parameters == "print")
                    {
                        return new PrintRoute(manager);
                    }
                    else if (parameters.StartsWith("find"))
                    {
                        string[] partsTreeCommand = fullCommand.Split(' ', 3);
                        string destinationAirport = partsTreeCommand[2];
                        return new FlightRouteSearchCommand(routeTree) { DestinationAirport = destinationAirport };
                    }
                    else if (parameters.StartsWith("search"))
                    {
                        string[] partsGraphCommand = fullCommand.Split(' ');
                        string startAirport = partsGraphCommand[2];
                        string endAirport = partsGraphCommand[3];
                        string strategy = partsGraphCommand[4];

                        return strategy switch
                        {
                            "cheap" => new FindShortestPathByPrice(flightGraph, startAirport, endAirport, flights),
                            "time" => new FindShortestPathByTime(flightGraph, startAirport, endAirport, flights),
                            "stops" => new FindShortestPathByStops(flightGraph, startAirport, endAirport),
                            _ => new CheckIfAirportsAreConnected(flightGraph, startAirport, endAirport),
                        };
                    }
                    else
                    {
                        Console.WriteLine("Invalid command.");
                        Console.WriteLine();
                        PrintInformation.PrintMenu();
                        Console.WriteLine();
                        return null;
                    }

                case "reserve":
                    if (parameters.StartsWith("cargo"))
                    {
                        Console.WriteLine("<Flight Identifier> <Cargo Weight> <Cargo Volume>");
                        string cargoReserveCommand = Console.ReadLine()!;
                        return new ReserveCargo(cargoAircraftDictionary, flights, cargoReserveCommand);
                    }
                    else if (parameters.StartsWith("ticket"))
                    {
                        Console.WriteLine("<Flight Identifier> <Seats> <Small Baggage Count> <Large Baggage Count>");
                        string passengerReservationCommand = Console.ReadLine()!;
                        return new ReservePassengerTicket(passengerAircraftDictionary, flights, passengerReservationCommand);
                    }
                    else
                    {
                        Console.WriteLine("Invalid command.");
                        Console.WriteLine();
                        PrintInformation.PrintMenu();
                        Console.WriteLine();
                        return null;
                    }

                default:
                    Console.WriteLine("Invalid command.");
                    Console.WriteLine();
                    PrintInformation.PrintMenu();
                    Console.WriteLine();
                    return null;
            }
            return null;
        }
    }
}