﻿using Airlines.Business.Services;
using Airlines.Persistance.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Airlines.API.Controllers;
[Route("api/[controller]")]
[ApiController]
public class AirportController : ControllerBase
{
    private readonly AirportService _airportService;

    public AirportController(AirportService airportService)
    {
        _airportService = airportService;
    }

    [HttpGet]
    public IActionResult GetAll()
    {
        List<Airport> airportList = _airportService.GetAirports();

        if (airportList.Count == 0)
        {
            return NotFound();
        }
        else if (airportList == null)
        {
            return StatusCode(500, new { message = "Internal server error." });
        }

        return Ok(airportList);
    }

    [HttpGet("{id}")]
    public IActionResult GetOne(int id)
    {
        var airport = _airportService.GetAirportById(id);

        if (airport == null)
        {
            return BadRequest($"Airport with id {id} was not found.");
        }

        return Ok(airport);
    }

    [HttpGet("filter/{filter}")]
    public IActionResult GetByFilter(string text)
    {
        var airport = _airportService.GetAirportByAnyField(text);

        if (airport == null)
        {
            return BadRequest($"Airport was not found.");
        }

        return Ok(airport);
    }

    [HttpPost]
    public IActionResult Create([FromBody] Airport newAirport)
    {
        var isCreated = _airportService.AddAirportToDB(newAirport);

        if (!isCreated)
        {
            return StatusCode(500, new { message = "Internal server error." });
        }

        return CreatedAtAction(nameof(GetAll), new { id = newAirport.Id }, newAirport);
    }

    [HttpPut]
    public IActionResult Update([FromBody] Airport airport)
    {
        var isUpdated = _airportService.UpdateAirport(airport);

        if (!isUpdated)
        {
            return StatusCode(500, new { message = "Internal server error." });
        }

        return Ok();
    }

    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
        var isDeleted = _airportService.DeleteAirport(id);

        if (!isDeleted)
        {
            return BadRequest($"Airline with id {id} was already deleted or was not found.");
        }

        return Ok();
    }
}
