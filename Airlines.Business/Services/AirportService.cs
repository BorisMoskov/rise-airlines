﻿using Airlines.Persistance.Models;
using Airlines.Persistence.Repositories;
using Airlines.Persistence.Repositories.Interfaces;

namespace Airlines.Business.Services;
public class AirportService
{
    private readonly IAirportRepository _airportRepository;

    public AirportService(IAirportRepository airportRepository)
    {
        _airportRepository = airportRepository;
    }

    public List<Airport> GetAirports()
    {
        List<Airport> airports = _airportRepository.GetAirports();
        return airports;
    }

    public Airport GetAirportById(int id)
    {
        Airport airport = _airportRepository.GetAirportById(id);
        return airport;
    }

    public List<Airport> GetAirportByAnyField(string text)
    {
        List<Airport> airport = _airportRepository.GetAirportByAnyField(text);
        return airport;
    }

    public bool AddAirportToDB(Airport airport)
    {
        bool success = _airportRepository.AddAirport(airport);
        return success;
    }

    public bool UpdateAirport(Airport airport)
    {
        bool success = _airportRepository.UpdateAirport(airport);
        return success;
    }

    public bool DeleteAirport(int id)
    {
        bool success = _airportRepository.DeleteAirport(id);
        return success;
    }

    public int GetAirportsCount()
    {
        return _airportRepository.GetCount();
    }

    public List<Airport> SearchInAirport(string searchTerm)
    {
        return _airportRepository.Search(searchTerm);
    }
}
