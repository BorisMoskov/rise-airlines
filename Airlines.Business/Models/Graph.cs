﻿
namespace Airlines.Business.Models;
public class GraphNode<T>(T data)
{
    public T Data { get; set; } = data;

    public List<T> Neighbours { get; set; } = [];

}
public class Graph<T>
{
    public Graph() => GraphNodes = [];

    public List<GraphNode<T>> GraphNodes { get; set; }

    internal void AddVertex(T data)
    {
        if (!ContainsNode(data))
        {
            GraphNode<T> node = new GraphNode<T>(data);
            GraphNodes.Add(node);
        }
    }
    internal void AddEdge(T source, T destination)
    {
        GraphNode<T>? sourceNode = GraphNodes.FirstOrDefault(node => EqualityComparer<T>.Default.Equals(node.Data, source));
        if (sourceNode != null)
        {
            if (!sourceNode.Neighbours.Any(neighbour => EqualityComparer<T>.Default.Equals(neighbour, destination)))
            {
                sourceNode.Neighbours.Add(destination);
            }
        }
    }
    internal void PrintGraph(Func<T, string> printFunction)
    {
        foreach (GraphNode<T> node in GraphNodes)
        {
            Console.Write($"Node: {printFunction(node.Data)}");

            Console.Write(" -> ");
            string neighboursString = string.Join(", ", node.Neighbours.Select(neighbour => printFunction(neighbour)));
            Console.WriteLine(neighboursString);
        }
    }

    internal GraphNode<T>? GetNode(T data) => GraphNodes.FirstOrDefault(node => EqualityComparer<T>.Default.Equals(node.Data, data));
    internal bool ContainsNode(T data) => GraphNodes.Any(node => EqualityComparer<T>.Default.Equals(node.Data, data));
}
