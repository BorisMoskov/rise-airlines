﻿
namespace Airlines.Business.Models;
public class TreeNode<T>(T data)
{
    public T Data { get; set; } = data;
    public List<TreeNode<T>> Children { get; } = [];

    public void AddChild(TreeNode<T> child) => Children.Add(child);
}
