﻿using Airlines.Persistance.Models;


namespace Airlines.Persistence.Repositories.Interfaces;
public interface IAirportRepository
{
    public List<Airport> GetAirports();
    public Airport GetAirportById(int id);
    public List<Airport> GetAirportByAnyField(string text);
    public bool AddAirport(Airport airport);
    public bool UpdateAirport(Airport airport);
    public bool DeleteAirport(int id);
    public int GetCount();
    public List<Airport> Search(string searchTerm);
}
