﻿
namespace Airlines.Business.CusomExceptions;
public class DataInitializationException : Exception
{
    public DataInitializationException() { }

    public DataInitializationException(string message)
        : base(message) { }

    public DataInitializationException(string message, Exception innerException)
        : base(message, innerException) { }
}
