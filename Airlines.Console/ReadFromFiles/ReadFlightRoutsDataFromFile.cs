﻿
namespace Airlines.Console.ReadFromFiles;
public class ReadFlightRoutsDataFromFile
{
    public static (string StartAirport, List<string> FlightIdentifiers) ReadFlightRoutes()
    {
        string filePath = "../../../InputData/FlightRoutesData.csv";
        try
        {
            var lines = File.ReadAllLines(filePath);
            if (lines.Length < 2)
            {
                throw new Exception("Invalid file format. File must contain at least two lines: Start Airport and Flight Identifiers.");
            }

            string startAirport = lines[0];
            var flightIdentifiers = new List<string>(lines[1..]);

            return (startAirport, flightIdentifiers);
        }
        catch
        {
            throw new Exception("Problem during reading from FlightRoutesData.csv");
        }
    }
}