﻿using Airlines.Persistance.Models;
using Airlines.Persistence.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Airlines.Persistence.Repositories;
public class FlightRepository : IFlightRepository
{
    private readonly Rise_DBContext _context;
    public FlightRepository(Rise_DBContext context)
    {
        _context = context;
    }

    public List<Flight> GetFlights()
    {
        return _context.Flights.ToList();
    }

    public Flight GetFlightById(int id)
    {
        try
        {
            return _context.Flights.FirstOrDefault(a => a.Id == id)!;
        }
        catch
        {
            return null!;
        }
    }

    public List<Flight> GetFlightsByAnyField(string text)
    {
        IQueryable<Flight> query = _context.Flights;

        if (!string.IsNullOrEmpty(text))
        {
            query = query.Where(flight =>
                    flight.FlightNumber.Contains(text) ||
                    flight.DepartureAirportId.ToString()!.Contains(text) ||
                    flight.ArrivalAirportId.ToString()!.Contains(text) ||
                    flight.DepartureDateTime.ToString()!.Contains(text) ||
                    flight.ArrivalDateTime.ToString()!.Contains(text));
        }

        return query.ToList();
    }

    public bool AddFlight(Flight flight)
    {
        try
        {
            _ = _context.Flights.Add(flight);
            _ = _context.SaveChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }

    public bool UpdateFlight(Flight flight)
    {
        try
        {
            _ = _context.Flights.Update(flight);
            _ = _context.SaveChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }

    public bool DeleteFlight(int id)
    {
        try
        {
            var flight = _context.Flights.FirstOrDefault(f => f.Id == id);
            if (flight != null)
            {
                _ = _context.Flights.Remove(flight);
                _ = _context.SaveChanges();
                return true;
            }
            return false; // If the flight with the given ID doesn't exist
        }
        catch
        {
            return false;
        }
    }

    public int GetCount()
    {
        return _context.Flights.Count();
    }

    public List<Flight> Search(string searchTerm)
    {
        List<Flight> flights = _context.Flights.ToList();
        List<Flight> results = new List<Flight>();

        if (searchTerm == null)
        {
            return flights;
        }

        foreach (var flight in flights)
        {
            if (MatchesSearchTerm(flight, searchTerm))
            {
                results.Add(flight);
            }
        }

        return results;
    }

    public static bool MatchesSearchTerm(Flight flight, string searchTerm)
    {
        if (int.TryParse(searchTerm, out int airportId))
        {
            return flight.DepartureAirportId == airportId || flight.ArrivalAirportId == airportId;
        }
        else
        {
            return flight.FlightNumber!.Contains(searchTerm, StringComparison.OrdinalIgnoreCase) ||
                   flight.DepartureAirportId.ToString()!.Contains(searchTerm, StringComparison.OrdinalIgnoreCase) ||
                   flight.ArrivalAirportId.ToString()!.Contains(searchTerm, StringComparison.OrdinalIgnoreCase) ||
                   flight.DepartureDateTime.ToString()!.Contains(searchTerm, StringComparison.OrdinalIgnoreCase) ||
                   flight.ArrivalDateTime.ToString()!.Contains(searchTerm, StringComparison.OrdinalIgnoreCase);
        }
    }
}