﻿using Airlines.Business.Commands;
using Airlines.Business.Models;

namespace Airlines.UnitTests;
public class ModelsTests
{
    [Fact]
    public void Aircraft_Should_Have_Correct_Model()
    {
        string model = "Boeing 747";

        var aircraft = new Aircraft(model);

        Assert.Equal(model, aircraft.Model);
    }

    [Fact]
    public void CargoAircraft_Should_Have_Correct_Properties()
    {
        string model = "Boeing 747";
        double cargoWeight = 10000;
        double cargoVolume = 5000;

        var cargoAircraft = new CargoAircraft(model, cargoWeight, cargoVolume);

        Assert.Equal(model, cargoAircraft.Model);
        Assert.Equal(cargoWeight, cargoAircraft.CargoWeight);
        Assert.Equal(cargoVolume, cargoAircraft.CargoVolume);
    }

    [Fact]
    public void PassengerAircraft_Should_Have_Correct_Properties()
    {
        string model = "Airbus A320";
        double cargoWeight = 5000;
        double cargoVolume = 3000;
        int seats = 150;

        var passengerAircraft = new PassengerAircraft(model, cargoWeight, cargoVolume, seats);

        Assert.Equal(model, passengerAircraft.Model);
        Assert.Equal(cargoWeight, passengerAircraft.CargoWeight);
        Assert.Equal(cargoVolume, passengerAircraft.CargoVolume);
        Assert.Equal(seats, passengerAircraft.Seats);
    }

    [Fact]
    public void PrivateAircraft_Should_Have_Correct_Properties()
    {
        string model = "Cessna Citation X";
        int seats = 8;

        var privateAircraft = new PrivateAircraft(model, seats);

        Assert.Equal(model, privateAircraft.Model);
        Assert.Equal(seats, privateAircraft.Seats);
    }

    [Fact]
    public void FindRoute_Should_Find_Route_Correctly()
    {
        var flightRouteTree = new FlightRouteTree("DFW");
        flightRouteTree.AddFlight("DFW", "JFK");
        flightRouteTree.AddFlight("JFK", "LAX");
        flightRouteTree.AddFlight("LAX", "ORD");
        flightRouteTree.AddFlight("ORD", "ROM");

        var route = flightRouteTree.FindRoute("ROM");

        Assert.Equal(["DFW", "JFK", "LAX", "ORD", "ROM"], route);
    }

    [Fact]
    public void FindRoute_Should_Return_Empty_Route_If_Destination_Not_Found()
    {
        var flightRouteTree = new FlightRouteTree("DFW");
        flightRouteTree.AddFlight("DFW", "JFK");
        flightRouteTree.AddFlight("JFK", "LAX");

        var route = flightRouteTree.FindRoute("ORD");

        Assert.Empty(route);
    }

    [Fact]
    public void Execute_Should_Handle_Non_Existent_Destination()
    {
        var flightRouteTree = new FlightRouteTree("DFW");
        flightRouteTree.AddFlight("DFW", "JFK");
        flightRouteTree.AddFlight("JFK", "LAX");
        var command = new FlightRouteSearchCommand(flightRouteTree) { DestinationAirport = "ORD" };

        var consoleOutput = ExecuteCommandAndGetConsoleOutput(command);

        Assert.Contains("No route to ORD found.", consoleOutput);
    }

    private static string ExecuteCommandAndGetConsoleOutput(FlightRouteSearchCommand command)
    {
        var consoleOutput = new StringWriter();
        Console.SetOut(consoleOutput);

        command.Execute();

        return consoleOutput.ToString();
    }

    [Fact]
    public void TreeNode_AddChild_Should_AddChildNode()
    {
        var parentNode = new TreeNode<int>(10);
        var childNode = new TreeNode<int>(20);

        parentNode.AddChild(childNode);

        _ = Assert.Single(parentNode.Children);
        Assert.Equal(childNode, parentNode.Children[0]);
    }

    [Theory]
    [InlineData("ABC", true)]
    [InlineData("ABCD", true)]
    [InlineData("A123", true)]
    [InlineData("1234", true)]
    [InlineData("A!B", false)]
    [InlineData("", false)]
    [InlineData("A", false)]
    [InlineData("ABCDE", false)]
    public void IsValidIdentifier_ValidatesIdentifier(string identifier, bool expected)
    {
        bool result = Airport.IsValidIdentifier(identifier);
        Assert.Equal(expected, result);
    }

    [Theory]
    [InlineData("Airport", true)]
    [InlineData("Airport.", true)]
    [InlineData("Airport City", true)]
    [InlineData("Airport123", false)]
    [InlineData("123", false)]
    [InlineData("Airport123@", false)]
    public void IsValidParameter_ValidatesParameter(string parameter, bool expected)
    {
        bool result = Airport.IsValidParameter(parameter);

        Assert.Equal(expected, result);
    }

    [Fact]
    public void Constructor_InvalidIdentifier_PrintsErrorMessage()
    {
        var consoleOutput = new StringWriter();
        Console.SetOut(consoleOutput);
        var identifier = "A";
        var name = "Airport";
        var city = "City";
        var country = "Country";

        _ = new Airport(identifier, name, city, country);

        Assert.Contains("", consoleOutput.ToString());
    }

    [Fact]
    public void Constructor_InvalidName_PrintsErrorMessage()
    {
        var consoleOutput = new StringWriter();
        Console.SetOut(consoleOutput);
        var identifier = "ABC";
        var name = "123";
        var city = "City";
        var country = "Country";

        _ = new Airport(identifier, name, city, country);

        Assert.Contains("", consoleOutput.ToString());
    }

    [Fact]
    public void SetIdentifier_ValidIdentifier_SetsIdentifier()
    {
        var airport = new Airport("ABC", "Airport", "City", "Country")
        {
            Identifier = "DEF"
        };

        Assert.Equal("DEF", airport.Identifier);
    }

    [Fact]
    public void SetName_ValidName_SetsName()
    {
        var airport = new Airport("ABC", "Airport", "City", "Country")
        {
            Name = "New Airport"
        };

        Assert.Equal("New Airport", airport.Name);
    }

    [Fact]
    public void SetCountry_ValidCountry_SetsCountry()
    {
        var airport = new Airport("ABC", "Airport", "City", "Country")
        {
            Country = "New Country"
        };

        Assert.Equal("New Country", airport.Country);
    }

    [Fact]
    public void SetCity_InvalidCity_PrintsErrorMessage()
    {
        var consoleOutput = new StringWriter();
        Console.SetOut(consoleOutput);
        _ = new Airport("ABC", "Airport", "City", "Country")
        {
            City = "123"
        };

        Assert.Contains("", consoleOutput.ToString());
    }

    [Fact]
    public void FlightProperties_InitializedCorrectly()
    {
        string identifier = "FL123";
        string departureAirportCode = "JFK";
        string arrivalAirportCode = "LAX";
        string aircraftModel = "Boeing 747";
        double price = 350;
        double time = 6;

        var flight = new Flight(identifier, departureAirportCode, arrivalAirportCode, aircraftModel, price, time);

        Assert.Equal(identifier, flight.Identifier);
        Assert.Equal(departureAirportCode, flight.DepartureAirport);
        Assert.Equal(arrivalAirportCode, flight.ArrivalAirport);
        Assert.Equal(aircraftModel, flight.AircraftModel);
        Assert.Equal(price, flight.Price);
        Assert.Equal(time, flight.Time);
    }

    [Theory]
    [InlineData("Delta Airlines")]
    [InlineData("British Airways")]
    [InlineData("Emirates")]
    [InlineData("Lufthansa")]
    public void IsValidName_ValidNames_ReturnsTrue(string name)
    {
        // Act
        bool result = Airline.IsValidName(name);

        // Assert
        Assert.True(result);
    }

    [Theory]
    [InlineData("123")]
    [InlineData("Airline123")]
    [InlineData("Delta Airlines!")]
    [InlineData("Name@")]
    public void IsValidName_InvalidNames_ReturnsFalse(string name)
    {
        // Act
        bool result = Airline.IsValidName(name);

        // Assert
        Assert.False(result);
    }
}