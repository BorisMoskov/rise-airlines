﻿using Airlines.Persistance.Models;
using Airlines.Persistence.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Airlines.Persistence.Repositories;
public class AirlineRepository : IAirlineRepository
{
    private readonly Rise_DBContext _context;
    public AirlineRepository(Rise_DBContext context)
    {
        _context = context;
    }
    public List<Airline> GetAirlines()
    {
        return _context.Airlines.ToList();
    }

    public Airline GetAirlineById(int id)
    {
        try
        {
            return _context.Airlines.FirstOrDefault(a => a.Id == id)!;
        }
        catch
        {
            return null!;
        }
    }

    public List<Airline> GetAirlinesByAnyField(string text)
    {
        IQueryable<Airline> query = _context.Airlines;

        if (!string.IsNullOrEmpty(text))
        {
            query = query.Where(airline =>
                    airline.Name.Contains(text) ||
                    airline.FleetSize.ToString()!.Contains(text) ||
                    airline.Founded.ToString()!.Contains(text) ||
                    airline.Description.Contains(text));
        }

        return query.ToList();
    }

    public bool AddAirline(Airline airline)
    {
        try
        {
            _ = _context.Airlines.Add(airline);
            _ = _context.SaveChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }

    public bool UpdateAirline(Airline airline)
    {
        try
        {
            _ = _context.Airlines.Update(airline);
            _ = _context.SaveChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }

    public bool DeleteAirline(int id)
    {
        try
        {
            var airline = _context.Airlines.FirstOrDefault(a => a.Id == id);
            if (airline != null)
            {
                _ = _context.Airlines.Remove(airline);
                _ = _context.SaveChanges();
                return true;
            }
            return false; // If the airline with the given ID doesn't exist
        }
        catch
        {
            return false;
        }
    }

    public int GetCount()
    {
        return _context.Airlines.Count();
    }

    public List<Airline> Search(string searchTerm)
    {
        List<Airline> airlines = _context.Airlines.ToList();
        List<Airline> results = new List<Airline>();

        if (searchTerm == null)
        {
            return airlines;
        }

        foreach (var airline in airlines)
        {
            if (MatchesSearchTerm(airline, searchTerm))
            {
                results.Add(airline);
            }
        }

        return results;
    }

    public static bool MatchesSearchTerm(Airline airline, string searchTerm)
    {
        if (int.TryParse(searchTerm, out int fleetSize))
        {
            // Search specifically in the FleetSize property
            return airline.FleetSize == fleetSize;
        }
        else
        {
            // Default search: check if any property of the airline matches the search term
            return airline.Name!.Contains(searchTerm, StringComparison.OrdinalIgnoreCase) ||
                   airline.Founded.ToString()!.Contains(searchTerm, StringComparison.OrdinalIgnoreCase) ||
                   airline.Description.Contains(searchTerm, StringComparison.OrdinalIgnoreCase);
        }
    }
}