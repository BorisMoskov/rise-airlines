document.addEventListener('DOMContentLoaded', function () {
    const form = document.getElementById('FlightForm');
    const submitButton = document.getElementById('submitButton');
    const errorMessages = document.getElementById('errorMessages');

    submitButton.disabled = true;

    const fields = [
        {
            id: 'flightNumber', validate: (value) => value.trim() === '' || value.length > 5,
            error: 'Flight number must be at most 5 characters long'
        },
        {
            id: 'departureAirportId', validate: (value) => value.trim() === '' || isNaN(value) || parseInt(value) <= 0,
            error: 'Departure Airport Id must be a number greater than 0'
        },
        {
            id: 'arrivalAirportId', validate: (value) => value.trim() === '' || isNaN(value) || parseInt(value) <= 0,
            error: 'Arrival Airport Id must be a number greater than 0'
        },
        {
            id: 'departureDateTime', validate: (value) => value.trim() === '' ||
                new Date(value) <= new Date(new Date().setHours(0, 0, 0, 0)),
            error: 'Departure date must be in the future'
        },
        {
            id: 'arrivalDateTime', validate: (value) => value.trim() === '' ||
                new Date(value) <= new Date(new Date().setHours(0, 0, 0, 0)),
            error: 'Arrival date must be in the future'
        }
    ];

    function validateField(field) {
        const fieldElement = document.getElementById(field.id);
        const errorMessage = fieldElement.nextElementSibling;

        const isValid = !field.validate(fieldElement.value);
        errorMessage.textContent = isValid ? '' : field.error;

        return isValid;
    }

    function validateForm() {
        const isFormValid = fields.every(validateField);
        submitButton.disabled = !isFormValid;
        errorMessages.textContent = isFormValid ? '' : 'Please fill in all required fields.';
    }

    function addValidationListener(fieldId) {
        const fieldElement = document.getElementById(fieldId.id);

        fieldElement.addEventListener('blur', function () {
            validateField(fieldId);
            validateForm();
        });

        fieldElement.addEventListener('input', () => {
            const errorMessage = fieldElement.nextElementSibling;
            errorMessage.textContent = '';
            validateForm();
        });
    }

    fields.forEach(addValidationListener);

    form.addEventListener('submit', function (event) {
        const isFormValid = fields.every(validateField);
        if (!isFormValid) {
            event.preventDefault();
            submitButton.disabled = true;
            errorMessages.textContent = 'Please fill in all required fields.';
        } else {
            errorMessages.textContent = '';
        }
    });
});
