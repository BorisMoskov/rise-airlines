﻿using Airlines.Business.Models;
using Airlines.Business;
using Airlines.Console.ReadFromFiles;
using Airlines.Business.Commands;
using Airlines.Business.CusomExceptions;
using Airlines.Business.CustomExceptions;
using System;

namespace Airlines.Console
{
    public class CommandManager
    {
        private bool _batchMode;
        private readonly Queue<string> _batchCommands = new();

        private HashSet<string> _airlines;
        private LinkedList<Flight> _flights;
        private Dictionary<string, Airport> _airports;
        private Dictionary<string, List<Airport>> _airportsByCity;
        private Dictionary<string, List<Airport>> _airportsByCountry;
        private Dictionary<string, CargoAircraft> _cargoAircraftDictionary;
        private Dictionary<string, PassengerAircraft> _passengerAircraftDictionary;
        private RouteManagement _manager;
        private string _startAirportCode;
        private List<string> _flightIdentifiers;
        private FlightRouteTree _routeTree;
        private Graph<string> _flightGraph;

#pragma warning disable CS8618
        public CommandManager() => InitializeData();

        private void InitializeData()
        {
            try
            {
                // Read data from files and initialize the data structures
                _airlines = ReadAirlinesFromFile.ReadFromFile();
                _flights = ReadFlightsFromFile.ReadFromFile();
                _airports = ReadAirportsFromFile.ReadFromFile();
                _airportsByCity = [];
                _airportsByCountry = [];
                _cargoAircraftDictionary = [];
                _passengerAircraftDictionary = [];
                _manager = new RouteManagement();
                _flightGraph = new Graph<string>();

                // Populate dictionaries
                foreach (var airport in _airports.Values)
                {
                    if (!_airportsByCity.TryGetValue(airport.City, out var value))
                    {
                        value = [];
                        _airportsByCity[airport.City] = value;
                    }

                    value.Add(airport);

                    if (!_airportsByCountry.TryGetValue(airport.Country, out value))
                    {
                        value = [];
                        _airportsByCountry[airport.Country] = value;
                    }

                    value.Add(airport);
                }

                ReadAircraftsFromFile.LoadAircraftDefinitions(_cargoAircraftDictionary, _passengerAircraftDictionary, []);

                // Construct flight route tree
                (_startAirportCode, _flightIdentifiers) = ReadFlightRoutsDataFromFile.ReadFlightRoutes();
                _routeTree = new FlightRouteTree(_startAirportCode);
                foreach (var flightIdentifier in _flightIdentifiers)
                {
                    var flight = _flights.FirstOrDefault(f => f.Identifier == flightIdentifier);
                    if (flight != null)
                    {
                        _routeTree.AddFlight(_startAirportCode, flight.ArrivalAirport);
                    }
                }

                foreach (var airport in _airports.Values)
                {
                    _flightGraph.AddVertex(airport.Identifier);
                }

                // Add edges for each flight
                foreach (var flight in _flights)
                {
                    _flightGraph.AddEdge(flight.DepartureAirport, flight.ArrivalAirport);
                }
            }
            catch (Exception ex)
            {
                throw new DataInitializationException("An error occurred while initializing data.", ex);
            }
        }

        public void ProcessCommand(string command)
        {
            try
            {
                if (command == null)
                {
                    return;
                }

                if (_batchMode)
                {
                    if (command == "batch run")
                    {
                        ExecuteBatchCommands();
                        _batchMode = false;
                    }
                    else if (command == "batch cancel")
                    {
                        _batchCommands.Clear();
                        _batchMode = false;
                    }
                    else
                    {
                        _batchCommands.Enqueue(command);
                    }
                }
                else
                {
                    if (command == "batch start")
                    {
                        _batchMode = true;
                        System.Console.WriteLine("Batch mode activated. Enter 'batch run' to execute commands in batch.");
                    }
                    else
                    {
                        ExecuteCommand(command);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new CommandProcessingException("An error occurred while processing command.", ex);
            }
        }

        private void ExecuteBatchCommands()
        {
            while (_batchCommands.Count > 0)
            {
                string nextCommand = _batchCommands.Dequeue();
                ExecuteCommand(nextCommand);
            }
        }

        private void ExecuteCommand(string command)
        {
            try
            {
                // Create the command using the CommandFactory
                ICommand? commandObject = CommandFactory.CreateCommand(_airports, _airportsByCity, _airportsByCountry,
                    _airlines, _flights, _manager, _cargoAircraftDictionary, _passengerAircraftDictionary,
                    _routeTree, _flightGraph, command);

                commandObject?.Execute();
            }
            catch
            {
                System.Console.WriteLine("Invalid command format");
                return;
            }
        }
    }
}