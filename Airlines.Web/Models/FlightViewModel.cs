﻿using Airlines.Persistance.Models;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Airlines.Web.Models;

public class FlightViewModel
{
    [Key]
    public int Id { get; set; }

    [StringLength(5)]
    [Unicode(false)]
    public string? FlightNumber { get; set; }
    public int? DepartureAirportId { get; set; }

    public int? ArrivalAirportId { get; set; }

    [Column(TypeName = "datetime")]
    public DateTime? DepartureDateTime { get; set; }

    [Column(TypeName = "datetime")]
    public DateTime? ArrivalDateTime { get; set; }

    public string? From { get; set; }
    public string? To { get; set; }

    [ForeignKey("ArrivalAirportId")]
    [InverseProperty("FlightArrivalAirports")]
    public virtual Airport? ArrivalAirport { get; set; }

    [ForeignKey("DepartureAirportId")]
    [InverseProperty("FlightDepartureAirports")]
    public virtual Airport? DepartureAirport { get; set; }
}
