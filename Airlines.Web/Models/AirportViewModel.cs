﻿using Airlines.Persistance.Models;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Airlines.Web.Models;

public class AirportViewModel
{
    [Key]
    [Column("ID")]
    public int Id { get; set; }

    [StringLength(256)]
    public string? Name { get; set; }

    [StringLength(256)]
    public string? Country { get; set; }

    [StringLength(256)]
    public string? City { get; set; }

    [StringLength(3)]
    [Unicode(false)]
    public string? Code { get; set; }

    public int? RunwaysCount { get; set; }

    public DateOnly? Founded { get; set; }

    [InverseProperty("ArrivalAirport")]
    public virtual ICollection<Flight> FlightArrivalAirports { get; set; } = new List<Flight>();

    [InverseProperty("DepartureAirport")]
    public virtual ICollection<Flight> FlightDepartureAirports { get; set; } = new List<Flight>();
}
