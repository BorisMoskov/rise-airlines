﻿using Airlines.Business.Models;
using AirlinesBusiness;
using System.Diagnostics.CodeAnalysis;

namespace Airlines.Business.Commands;
[ExcludeFromCodeCoverage]
public class ReservePassengerTicket(Dictionary<string, PassengerAircraft> passengerAircraft, LinkedList<Flight> flights, string command) : ICommand
{
    public void Execute()
    {
        string[] commandParts;
        string flightIdentifier;
        int seats;
        double smallBaggageCount;
        double largeBaggageCount;

        try
        {
            commandParts = [.. command.Split(' ')];
            flightIdentifier = commandParts[0];
            seats = int.Parse(commandParts[1]);
            smallBaggageCount = double.Parse(commandParts[2]);
            largeBaggageCount = double.Parse(commandParts[3]);
        }
        catch
        {
            Console.WriteLine("Incorrect command format");
            Console.WriteLine("<Flight Identifier> <Seats> <Small Baggage Count> <Large Baggage Count>");
            return;
        }

        const int smallBaggageWeight = 15; //15kg
        const double smallBaggageVolume = 0.045; //0.045 m³
        const int largeBaggageWeight = 30; //30kg
        const double largeBaggageVolume = 0.090; //0.090 m³

        double totalWeight = (smallBaggageCount * smallBaggageWeight) + (largeBaggageCount * largeBaggageWeight);
        double totalVolume = (smallBaggageCount * smallBaggageVolume) + (largeBaggageCount * largeBaggageVolume);

        Flight? flight = CheckIfFlightExist(flights, flightIdentifier);

        if (flight != null)
        {
            foreach (var aircraft in passengerAircraft)
            {
                if (seats <= aircraft.Value.Seats && totalWeight <= aircraft.Value.CargoWeight && totalVolume <= aircraft.Value.CargoVolume)
                {
                    PrintInformation.PrintReservationInfo(flightIdentifier);
                    return;
                }
            }
            Console.WriteLine("Booked unsuccessfully");
        }
        else
        {
            Console.WriteLine("No such flight exists");
        }
    }

    internal static Flight? CheckIfFlightExist(LinkedList<Flight> flights, string identifier)
    {
        foreach (var flight in flights)
        {
            if (flight.Identifier == identifier)
            {
                return flight;
            }
        }
        return null;
    }
}