﻿using Airlines.Business.Models;
using AirlinesBusiness;
using System.Diagnostics.CodeAnalysis;

namespace Airlines.Business.Commands;
[ExcludeFromCodeCoverage]
internal class CheckIfAirportsAreConnected(Graph<string> flightGraph, string startAirport, string endAirport) : ICommand
{
    private readonly Graph<string> _flightGraph = flightGraph;
    private readonly string _startAirport = startAirport;
    private readonly string _endAirport = endAirport;

    public void Execute()
    {
        bool isConnected = AreAirportsConnected(_startAirport, _endAirport, out List<string> path);

        PrintInformation.PrintRoutePath(isConnected, path);
    }

    private bool AreAirportsConnected(string startAirport, string endAirport, out List<string> path)
    {
        Dictionary<string, int> distances = [];
        Dictionary<string, string> parentMap = [];
        HashSet<string> visited = [];

        // Initialize distances to airports with infinity
        foreach (var node in _flightGraph.GraphNodes)
        {
            distances[node.Data] = int.MaxValue;
        }

        // Initialize the start airport distance to 0
        distances[startAirport] = 0;

        // Initialize priority queue for Dijkstra's algorithm
        PriorityQueue<Tuple<string, int>, int> queue = new PriorityQueue<Tuple<string, int>, int>();
        queue.Enqueue(new Tuple<string, int>(startAirport, 0), 0);

        while (queue.Count > 0)
        {
            var (currentAirport, distance) = queue.Dequeue();

            if (visited.Contains(currentAirport)) continue;

            _ = visited.Add(currentAirport);

            foreach (var node in _flightGraph.GraphNodes)
            {
                if (node.Data == currentAirport)
                {
                    foreach (var neighbour in node.Neighbours)
                    {
                        int newDistance = distance + 1; // Assuming all edges have weight 1

                        if (newDistance < distances[neighbour])
                        {
                            distances[neighbour] = newDistance;
                            parentMap[neighbour] = currentAirport;
                            queue.Enqueue(new Tuple<string, int>(neighbour, newDistance), newDistance);
                        }
                    }
                }
            }
        }

        // Reconstruct the shortest path
        path = [];
        if (distances.TryGetValue(endAirport, out var value) && value != int.MaxValue)
        {
            string airport = endAirport;
            while (airport != startAirport)
            {
                path.Add(airport);
                airport = parentMap[airport];
            }
            path.Add(startAirport);
            path.Reverse();
            return true; // Path found
        }
        else
        {
            // No path found
            return false;
        }
    }
}