﻿using Airlines.Business.CusomExceptions;
using Airlines.Business.CustomExceptions;

namespace Airlines.UnitTests;
public class ExceptionsClass
{
    [Fact]
    public void CommandExecutionException_Message_ShouldBeSet()
    {
        string expectedMessage = "Test Exception Message";

        var exception = new CommandExecutionException(expectedMessage);

        Assert.Equal(expectedMessage, exception.Message);
    }

    [Fact]
    public void CommandExecutionException_InnerException_ShouldBeSet()
    {
        string expectedInnerMessage = "Inner Exception Message";
        var innerException = new Exception(expectedInnerMessage);

        var exception = new CommandExecutionException("Test Exception Message", innerException);

        Assert.Equal(expectedInnerMessage, exception.InnerException?.Message);
    }

    [Fact]
    public void CommandProcessingException_Message_ShouldBeSet()
    {
        string expectedMessage = "Test Exception Message";

        var exception = new CommandProcessingException(expectedMessage);

        Assert.Equal(expectedMessage, exception.Message);
    }

    [Fact]
    public void CommandProcessingException_InnerException_ShouldBeSet()
    {
        string expectedInnerMessage = "Inner Exception Message";
        var innerException = new Exception(expectedInnerMessage);

        var exception = new CommandProcessingException("Test Exception Message", innerException);

        Assert.Equal(expectedInnerMessage, exception.InnerException?.Message);
    }

    [Fact]
    public void CommandSplitException_Message_ShouldBeSet()
    {
        string expectedMessage = "Test Exception Message";

        var exception = new CommandSplitException(expectedMessage);

        Assert.Equal(expectedMessage, exception.Message);
    }

    [Fact]
    public void CommandSplitException_InnerException_ShouldBeSet()
    {
        string expectedInnerMessage = "Inner Exception Message";
        var innerException = new Exception(expectedInnerMessage);

        var exception = new CommandSplitException("Test Exception Message", innerException);

        Assert.Equal(expectedInnerMessage, exception.InnerException?.Message);
    }

    [Fact]
    public void DataInitializationException_Message_ShouldBeSet()
    {
        string expectedMessage = "Test Exception Message";

        var exception = new DataInitializationException(expectedMessage);

        Assert.Equal(expectedMessage, exception.Message);
    }

    [Fact]
    public void DataInitializationException_InnerException_ShouldBeSet()
    {
        string expectedInnerMessage = "Inner Exception Message";
        var innerException = new Exception(expectedInnerMessage);

        var exception = new DataInitializationException("Test Exception Message", innerException);

        Assert.Equal(expectedInnerMessage, exception.InnerException?.Message);
    }

    [Fact]
    public void ParsingException_Message_ShouldBeSet()
    {
        string expectedMessage = "Test Exception Message";

        var exception = new ParsingException(expectedMessage);

        Assert.Equal(expectedMessage, exception.Message);
    }

    [Fact]
    public void ParsingException_InnerException_ShouldBeSet()
    {
        string expectedInnerMessage = "Inner Exception Message";
        var innerException = new Exception(expectedInnerMessage);

        var exception = new ParsingException("Test Exception Message", innerException);

        Assert.Equal(expectedInnerMessage, exception.InnerException?.Message);
    }
}
