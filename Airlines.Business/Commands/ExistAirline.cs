﻿using AirlinesBusiness;
using System.Diagnostics.CodeAnalysis;

namespace Airlines.Business.Commands;
[ExcludeFromCodeCoverage]
public class ExistAirline(HashSet<string> airlines, string command) : ICommand
{
    public void Execute()
    {
        string[] commandParts;
        string airlineName;
        try
        {
            commandParts = [.. command.Split(' ')];
            airlineName = commandParts[1];
        }
        catch
        {
            Console.WriteLine("Incorrect command format");
            Console.WriteLine("exist <airline name>");
            return;
        }

        bool isExist = airlines.Contains(airlineName);
        PrintInformation.PrintIfAirlineExist(isExist, airlineName);
    }
}
