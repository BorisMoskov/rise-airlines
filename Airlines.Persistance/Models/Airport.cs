﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Airlines.Persistance.Models;

[Table("Airport")]
public partial class Airport
{
    [Key]
    [Column("ID")]
    public int Id { get; set; }

    [StringLength(100, ErrorMessage = "The {0} must be at most {1} characters long.")]
    [Required(ErrorMessage = "The {0} field is required.")]
    public string Name { get; set; }

    [StringLength(30, ErrorMessage = "The {0} must be at most {1} characters long.")]
    [Required(ErrorMessage = "The {0} field is required.")]
    public string Country { get; set; }

    [StringLength(30, ErrorMessage = "The {0} must be at most {1} characters long.")]
    [Required(ErrorMessage = "The {0} field is required.")]
    public string City { get; set; }

    [StringLength(3, ErrorMessage = "The {0} must be at most {1} characters long.")]
    [Required(ErrorMessage = "The {0} field is required.")]
    [RegularExpression("^[A-Z]+$", ErrorMessage = "The {0} field must be uppercase alphabetic characters only.")]
    public string Code { get; set; }

    [Range(1, int.MaxValue, ErrorMessage = "The {0} must be at least {1}.")]
    [Required(ErrorMessage = "The {0} field is required.")]
    public int? RunwaysCount { get; set; }

    [DataType(DataType.Date)]
    [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
    [Required(ErrorMessage = "The {0} field is required.")]
    public DateOnly? Founded { get; set; }

    [InverseProperty("ArrivalAirport")]
    public virtual ICollection<Flight> FlightArrivalAirports { get; set; } = new List<Flight>();

    [InverseProperty("DepartureAirport")]
    public virtual ICollection<Flight> FlightDepartureAirports { get; set; } = new List<Flight>();
}