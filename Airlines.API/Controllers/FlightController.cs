﻿using Airlines.Business.Services;
using Airlines.Persistance.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Airlines.API.Controllers;
[Route("api/[controller]")]
[ApiController]
public class FlightController : ControllerBase
{
    private readonly FlightService _flightService;

    public FlightController(FlightService flightService)
    {
        _flightService = flightService;
    }

    [HttpGet]
    public IActionResult GetAll()
    {
        List<Flight> flightList = _flightService.GetFlights();

        if (flightList.Count == 0)
        {
            return NotFound();
        }
        else if (flightList == null)
        {
            return StatusCode(500, new { message = "Internal server error." });
        }

        return Ok(flightList);
    }

    [HttpGet("{id}")]
    public IActionResult GetOne(int id)
    {
        var flight = _flightService.GetFlightById(id);

        if (flight == null)
        {
            return BadRequest($"Flight with id {id} was not found.");
        }

        return Ok(flight);
    }

    [HttpGet("filter/{filter}")]
    public IActionResult GetByFilter(string text)
    {
        var flight = _flightService.GetFlightsByAnyField(text);

        if (flight == null)
        {
            return BadRequest($"Flight was not found.");
        }

        return Ok(flight);
    }

    [HttpPost]
    public IActionResult Create([FromBody] Flight newFlight)
    {
        var isCreated = _flightService.AddFlightToDB(newFlight);

        if (!isCreated)
        {
            return StatusCode(500, new { message = "Internal server error." });
        }

        return CreatedAtAction(nameof(GetAll), new { id = newFlight.Id }, newFlight);
    }

    [HttpPut]
    public IActionResult Update([FromBody] Flight flight)
    {
        var isUpdated = _flightService.UpdateFlight(flight);

        if (!isUpdated)
        {
            return StatusCode(500, new { message = "Internal server error." });
        }

        return Ok();
    }

    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
        var isDeleted = _flightService.DeleteFlight(id);

        if (!isDeleted)
        {
            return BadRequest($"Flight with id {id} was already deleted or was not found.");
        }

        return Ok();
    }
}
