﻿using Airlines.Business.CustomExceptions;
using Airlines.Business.Models;
using AirlinesBusiness;
using System.Diagnostics.CodeAnalysis;

namespace Airlines.Business.Commands;
[ExcludeFromCodeCoverage]
public class SortSpecificData(Dictionary<string, Airport> airports, HashSet<string> airlines, LinkedList<Flight> flights, string command) : ICommand
{
    public void Execute()
    {
        string[] commandParts;
        string array;
        string order;

        try
        {
            commandParts = command.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            array = commandParts[1].ToLower();
            order = "ascending";
        }
        catch (Exception ex)
        {
            Console.WriteLine("Incorrect command format");
            Console.WriteLine("sort <input data> [order]");
            return;
            throw new CommandSplitException("An error occurred while splitting the command.", ex);
        }

        if (commandParts.Length >= 3)
        {
            order = commandParts[2].ToLower();
        }

        switch (array)
        {
            case "airports":
                Dictionary<string, Airport> sortedAirports = order.Equals("ascending", StringComparison.CurrentCultureIgnoreCase) ?
                airports.OrderBy(kv => kv.Key).ToDictionary(kv => kv.Key, kv => kv.Value) :
                airports.OrderByDescending(kv => kv.Key).ToDictionary(kv => kv.Key, kv => kv.Value);

                PrintInformation.PrintAirports(sortedAirports);
                break;
            case "airlines":
                HashSet<string> sortedAirlines = order.Equals("ascending", StringComparison.CurrentCultureIgnoreCase) ?
                    [.. airlines.OrderBy(airline => airline)] :
                    [.. airlines.OrderByDescending(airline => airline)];

                PrintInformation.PrintAirlines(sortedAirlines);
                break;
            case "flights":
                LinkedList<Flight> sortedFlights = order.Equals("ascending", StringComparison.OrdinalIgnoreCase) ?
                    new LinkedList<Flight>(flights.OrderBy(flight => flight.Identifier)) :
                    new LinkedList<Flight>(flights.OrderByDescending(flight => flight.Identifier));

                PrintInformation.PrintFlights(sortedFlights);

                break;
            default:
                Console.WriteLine("Invalid option");
                break;
        }
    }
}