﻿using Airlines.Business.Models;

namespace Airlines.Business;
public class RouteManagement
{
    public readonly LinkedList<Flight> _route;
    public readonly Dictionary<string, (string, string, string)> _flightDict;

    public RouteManagement()
    {
        _route = new LinkedList<Flight>();
        _flightDict = [];
    }

    internal void NewRoute()
    {
        _route.Clear();
        _flightDict.Clear();
    }

    internal void AddFlight(string command, LinkedList<Flight> flights)
    {
        // Split command into parts
        string[] commandParts = command.Split(' ', StringSplitOptions.RemoveEmptyEntries);

        // Check if commandParts contains at least 3 elements
        if (commandParts.Length < 3)
        {
            Console.WriteLine("Invalid command format. Format: route add <Flight Identifier>");
            return;
        }

        string commandFlightIdentifier = commandParts[2];

        Flight newFlight = CheckIfFlightExist(flights, commandFlightIdentifier)!;
        if (newFlight != null)
        {
            string identifier = newFlight.Identifier;
            string departureAirport = newFlight.DepartureAirport;
            string arrivalAirport = newFlight.ArrivalAirport;
            string aircraftModel = newFlight.AircraftModel;
            // Check if the flight identifier already exists in dict
            if (!_flightDict.ContainsKey(identifier))
            {
                // Check if the route is empty or the new flight connects logically to the route
                if (_route.Count == 0 || _route.Last!.Value.ArrivalAirport == departureAirport)
                {
                    _flightDict.Add(identifier, (departureAirport, arrivalAirport, aircraftModel));
                    _ = _route.AddLast(newFlight);
                    Console.WriteLine($"Flight {identifier} added to the route.");
                }
                else
                {
                    Console.WriteLine($"Error: Flight {identifier} cannot be added. It does not connect logically to the route.");
                }
            }
            else
            {
                Console.WriteLine($"Flight {identifier} already exists in the route.");
            }
        }
        else
        {
            Console.WriteLine("No such flight exists");
        }
    }

    internal void RemoveLastFlight()
    {
        if (_route.Count > 0)
        {
            Flight lastFlight = _route.Last!.Value;
            _route.RemoveLast();
            _ = _flightDict.Remove(lastFlight.Identifier);
            Console.WriteLine($"Removed last flight from the route: {lastFlight.Identifier}");
        }
        else
        {
            Console.WriteLine("Route is empty. No flight to remove.");
        }
    }

    internal static Flight? CheckIfFlightExist(LinkedList<Flight> flights, string identifier)
    {
        foreach (var flight in flights)
        {
            if (flight.Identifier == identifier)
            {
                return flight;
            }
        }
        return null;
    }

    public void PrintRoute()
    {
        Console.WriteLine();
        Console.WriteLine("Route:");
        foreach (var flight in _route)
        {
            Console.WriteLine($"Flight {flight.Identifier}, Model {flight.AircraftModel}: {flight.DepartureAirport} -> {flight.ArrivalAirport}");
        }
        Console.WriteLine();
    }
}
