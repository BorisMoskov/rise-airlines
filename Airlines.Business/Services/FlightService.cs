﻿using Airlines.Persistance.Models;
using Airlines.Persistence.Repositories;
using Airlines.Persistence.Repositories.Interfaces;

namespace Airlines.Business.Services;
public class FlightService
{
    private readonly IFlightRepository _flightRepository;

    public FlightService(IFlightRepository flightRepository)
    {
        _flightRepository = flightRepository;
    }

    public List<Flight> GetFlights()
    {
        List<Flight> flights = _flightRepository.GetFlights();
        return flights;
    }

    public Flight GetFlightById(int id)
    {
        Flight flight = _flightRepository.GetFlightById(id);
        return flight;
    }

    public List<Flight> GetFlightsByAnyField(string text)
    {
        List<Flight> flight = _flightRepository.GetFlightsByAnyField(text);
        return flight;
    }

    public bool AddFlightToDB(Flight flight)
    {
        bool success = _flightRepository.AddFlight(flight);
        return success;
    }

    public bool UpdateFlight(Flight flight)
    {
        bool success = _flightRepository.UpdateFlight(flight);
        return success;
    }

    public bool DeleteFlight(int id)
    {
        bool success = _flightRepository.DeleteFlight(id);
        return success;
    }

    public int GetFlightsCount()
    {
        return _flightRepository.GetCount();
    }

    public List<Flight> SearchInFlight(string searchTerm)
    {
        return _flightRepository.Search(searchTerm);
    }
}
