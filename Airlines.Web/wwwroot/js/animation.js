﻿function showLoader() {
    var loaderContainer = document.querySelector('.loader-container');
    var contentContainer = document.querySelector('.container-airlines');
    loaderContainer.classList.add('appear');
    contentContainer.classList.remove('hide-content');
}

function hideLoader() {
    var loaderContainer = document.querySelector('.loader-container');
    loaderContainer.classList.remove('appear');
}

document.addEventListener('DOMContentLoaded', function () {
    showLoader();

    setTimeout(function () {
        hideLoader();
    }, 1000);
});