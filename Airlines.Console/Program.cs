﻿using Airlines.Console;
using AirlinesBusiness;
using Airlines.Persistence.Repositories;
using Airlines.Persistance.Models;
using System.Diagnostics.Metrics;

namespace AirlinesConsole
{
    public class Program
    {
        private static void Main()
        {
            /*RiseDBTesting riseDBTesting = new RiseDBTesting();

            // List all data
            riseDBTesting.ListFlights();
            riseDBTesting.ListAirlines();
            riseDBTesting.ListAirports();

            Console.WriteLine();

            //List filtered airlines
            static bool flightFilter(Flight flight)
            {
                return flight.ArrivalAirportId == 3;
            }

            var flights = riseDBTesting.ListFlights(flightFilter);

            foreach (var flight in flights)
            {
                Console.WriteLine($"Flight ID: {flight.Id}");
            }

            Console.WriteLine();

            //List filtered airlines
            static bool airlineFilter(Airline airline)
            {
                return airline.FleetSize > 20;
            }

            var airlines = riseDBTesting.ListAirlines(airlineFilter);

            foreach (var airline in airlines)
            {
                Console.WriteLine($"Airline Name: {airline.Name}, Fleet size: {airline.FleetSize}");
            }

            Console.WriteLine();

            //List filtered airports
            static bool airportFilter(Airport airport)
            {
                return airport.Country == "United States";
            }

            var airports = riseDBTesting.ListAirports(airportFilter);

            foreach (var airport in airports)
            {
                Console.WriteLine($"Airport Name: {airport.Name}, Country: {airport.Country}");
            }

            Console.WriteLine();

            ////////////////////////////  FLIGHT
            // Add flight
            var newFlight = new Flight
            {
                FlightNumber = "XY123",
                DepartureAirportId = 1,
                ArrivalAirportId = 2,
                DepartureDateTime = DateTime.Now,
                ArrivalDateTime = DateTime.Now.AddHours(2)
            };

            riseDBTesting.AddFlight(newFlight);

            // Update flight
            var flightToUpdate = new Flight
            {
                FlightNumber = "XY123",
                DepartureAirportId = 2, // New departure airport ID
                ArrivalAirportId = 3, // New arrival airport ID
                DepartureDateTime = DateTime.Now,
                ArrivalDateTime = DateTime.Now.AddHours(2)
            };

            riseDBTesting.UpdateFlight(flightToUpdate);

            // Delete flight
            riseDBTesting.DeleteFlight(newFlight);


            ///////////////////////////  AIRLINE
            // Add airline
            var newAirline = new Airline
            {
                Name = "Quatar Airlines",
                Founded = new DateOnly(2002, 1, 1),
                FleetSize = 20,
                Description = "Qatar Airways is a leading global airline renowned for its luxury service, modern fleet, and extensive international network."
            };
            riseDBTesting.AddAirline(newAirline);

            // Update airline
            var airlineToUpdate = new Airline
            {
                Name = "Quatar Airlines",
                Founded = new DateOnly(2002, 1, 1),
                FleetSize = 50,
                Description = "Qatar Airways is a leading global airline renowned for its luxury service, modern fleet, and extensive international network."
            };

            riseDBTesting.UpdateAirline(airlineToUpdate);

            // Delete airline
            riseDBTesting.DeleteAirline(newAirline);


            //////////////////////////  AIRPORT
            // Add airport
            var newAirport = new Airport
            {
                Name = "New Airport",
                Country = "New Country",
                City = "New City"
            };
            riseDBTesting.AddAirport(newAirport);

            // Update airport
            var airportToUpdate = new Airport
            {
                Name = "Not new Airport", //update name
                Country = "New Country",
                City = "New City"
            };
            riseDBTesting.UpdateAirport(airportToUpdate);

            // Delete airport
            riseDBTesting.DeleteAirport(newAirport);


            //For readability
            Console.WriteLine();

            PrintInformation.PrintMenu();
            Console.WriteLine();

            CommandManager commandManager = new CommandManager();
            string command;
            do
            {
                Console.Write("Enter command: ");
                command = Console.ReadLine()?.Trim()!;

                if (command.Equals("done", StringComparison.CurrentCultureIgnoreCase))
                {
                    break;
                }

                commandManager.ProcessCommand(command);

            } while (command != "done");*/
        }
    }
}
