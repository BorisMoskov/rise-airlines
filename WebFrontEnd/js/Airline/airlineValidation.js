document.addEventListener('DOMContentLoaded', function () {
    const form = document.getElementById('airlineForm');
    const submitButton = document.getElementById('submitButton');
    const errorMessages = document.getElementById('errorMessages');

    submitButton.disabled = true;

    const fields = [
        { id: 'name', error: 'Name must be at most 100 characters long', validate: (value) => value.trim() === '' || value.length > 100 },
        { id: 'founded', error: "Founded date must be less than or equal to today's date", validate: (value) => new Date(value) > new Date() },
        { id: 'fleet_size', error: 'Fleet size must be at least 1', validate: (value) => value <= 0 },
        { id: 'description', error: 'Description must be at least 10 symbols', validate: (value) => value.length < 10 }
    ];

    function validateField(field) {
        const fieldId = document.getElementById(field.id);
        const errorMessage = fieldId.nextElementSibling;

        const isValid = !field.validate || !field.validate(fieldId.value);
        errorMessage.textContent = isValid ? '' : field.error;

        return isValid;
    }

    function validateForm() {
        const isValid = fields.every(validateField);
        submitButton.disabled = !isValid;
        errorMessages.textContent = isValid ? '' : 'Please fill in all required fields.';
    }

    function addValidationListeners() {
        fields.forEach(field => {
            const fieldId = document.getElementById(field.id);

            fieldId.addEventListener('blur', validateForm);
            fieldId.addEventListener('input', () => {
                const errorMessage = fieldId.nextElementSibling;
                errorMessage.textContent = '';
                validateForm();
            });
        });
    }

    addValidationListeners();

    form.addEventListener('submit', function (event) {
        const isFormValid = fields.every(validateField);
        if (!isFormValid) {
            event.preventDefault();
            submitButton.disabled = true;
        }
    });
});
