﻿using Airlines.Persistance.Models;
using Airlines.Persistence.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Airlines.Business.Services;

namespace Airlines.Web.Controllers;
public class AirportController : Controller
{
    private readonly AirportService _airportService;
    public AirportController(AirportService airportService)
    {
        _airportService = airportService;
    }
    public IActionResult Index()
    {
        List<Airport> airportList = _airportService.GetAirports();
        return View(airportList);
    }

    [HttpPost]
    public ActionResult CreateNewAirport(Airport airport)
    {
        if (airport.Founded.HasValue && airport.Founded.Value > DateOnly.FromDateTime(DateTime.Today))
        {
            ModelState.AddModelError("Founded", "Invalid date.");
        }
        if (ModelState.IsValid)
        {
            _airportService.AddAirportToDB(airport);
            TempData["SuccessMessage"] = "Airport added successfully";
            return RedirectToAction(nameof(Index));
        }
        TempData["ErrorMessage"] = "The Airport was not added";
        List<Airport> airportList = _airportService.GetAirports();
        return View("Index", airportList);
    }

    public IActionResult Search(string searchTerm)
    {
        List<Airport> airports = _airportService.SearchInAirport(searchTerm);
        return View("Index", airports);
    }
}
