﻿namespace Airlines.Business.Models;

public class FlightRouteTree(string startAirport)
{
    public TreeNode<string> Root { get; } = new TreeNode<string>(startAirport);

    public void AddFlight(string source, string destination) => AddFlightRecursive(Root, source, destination);

    private static void AddFlightRecursive(TreeNode<string> node, string source, string destination)
    {
        if (node.Data == source)
        {
            node.AddChild(new TreeNode<string>(destination));
        }
        else
        {
            foreach (var child in node.Children)
            {
                AddFlightRecursive(child, source, destination);
            }
        }
    }

    public List<string> FindRoute(string destination)
    {
        var route = new List<string>();
        if (!FindRouteRecursive(Root, destination, route))
        {
            route.Clear();
        }
        return route;
    }

    private static bool FindRouteRecursive(TreeNode<string> node, string destination, List<string> route)
    {
        if (node == null)
        {
            return false;
        }

        // Add the current airport to the route
        route.Add(node.Data);

        if (node.Data == destination)
        {
            return true;
        }

        foreach (var child in node.Children)
        {
            if (FindRouteRecursive(child, destination, route))
            {
                return true;
            }
        }

        return false;
    }
}