﻿using Airlines.Persistance.Models;


namespace Airlines.Persistence.Repositories.Interfaces;
public interface IFlightRepository
{
    public List<Flight> GetFlights();
    public Flight GetFlightById(int id);
    public List<Flight> GetFlightsByAnyField(string text);
    public bool AddFlight(Flight flight);
    public bool UpdateFlight(Flight flight);
    public bool DeleteFlight(int id);
    public int GetCount();
    public List<Flight> Search(string searchTerm);
}
