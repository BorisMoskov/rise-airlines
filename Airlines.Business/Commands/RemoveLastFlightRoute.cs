﻿
using System.Diagnostics.CodeAnalysis;

namespace Airlines.Business.Commands;
[ExcludeFromCodeCoverage]
public class RemoveLastFlightRoute() : ICommand
{
    private readonly RouteManagement _manager = new();

    public void Execute() => _manager.RemoveLastFlight();
}