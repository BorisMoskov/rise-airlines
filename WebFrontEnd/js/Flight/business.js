import { fetchFlights, createFlight, deleteFlight, updateFlight } from './httpRequests.js';

document.addEventListener('DOMContentLoaded', () => {
    initialize();
});

function initialize() {
    loadFlights();
    setupEventListeners();
}

async function loadFlights() {
    try {
        const flights = await fetchFlights();
        populateFlightsTable(flights);
    } catch (error) {
        console.error('Failed to load flights:', error);
    }
}

function populateFlightsTable(flights) {
    const tableBody = document.getElementById('flightTableBody');
    tableBody.innerHTML = '';

    flights.forEach((flight) => {
        const row = document.createElement('tr');
        row.innerHTML = `
            <td>${flight.flightNumber}</td>
            <td>${flight.departureAirportId}</td>
            <td>${flight.arrivalAirportId}</td>
            <td>${flight.departureDateTime}</td>
            <td>${flight.arrivalDateTime}</td>
            <td>
                <button class="edit-button" data-id="${flight.id}">Edit</button>
                <button class="delete-button" data-id="${flight.id}">Delete</button>
            </td>
        `;
        tableBody.appendChild(row);
    });

    document.querySelectorAll('.delete-button').forEach(button => {
        button.addEventListener('click', async (event) => {
            const id = event.target.getAttribute('data-id');
            try {
                await deleteFlight(id);
                location.reload();
            } catch (error) {
                console.error('Failed to delete flight:', error);
                location.reload();
            }
        });
    });

    document.querySelectorAll('.edit-button').forEach(button => {
        button.addEventListener('click', (event) => {
            const id = event.target.getAttribute('data-id');
            editFlight(id);
        });
    });
}

function setupEventListeners() {
    const form = document.getElementById('flightForm');
    form.addEventListener('submit', async function(event) {
        event.preventDefault();
        
        const flightNumber = document.getElementById('flightNumber').value;
        const departureAirportId = document.getElementById('departureAirportId').value;
        const arrivalAirportId = document.getElementById('arrivalAirportId').value;
        const departureDateTime = document.getElementById('departureDateTime').value;
        const arrivalDateTime = document.getElementById('arrivalDateTime').value;
        
        const newFlight = {
            flightNumber,
            departureAirportId,
            arrivalAirportId,
            departureDateTime,
            arrivalDateTime
        };

        try {
            await createFlight(newFlight);
            location.reload();
        } catch (error) {
            console.error('Failed to create flight:', error);
            location.reload();
        }
    });
}

async function editFlight(id) {
    const flightForm = document.getElementById('flightForm');
    const editFlightForm = document.getElementById('editFlightForm');

    flightForm.style.display = 'none';
    editFlightForm.style.display = 'block';

    const flight = Array.from(document.querySelectorAll(`#flightTableBody tr`)).find(row => row.querySelector('.edit-button').getAttribute('data-id') === id);

    document.getElementById('editId').value = id;
    document.getElementById('editFlightNumber').value = flight.cells[0].textContent;
    document.getElementById('editDepartureAirportId').value = flight.cells[1].textContent;
    document.getElementById('editArrivalAirportId').value = flight.cells[2].textContent;
    document.getElementById('editDepartureDateTime').value = flight.cells[3].textContent;
    document.getElementById('editArrivalDateTime').value = flight.cells[4].textContent;

    const editSubmitButton = document.getElementById('editSubmitButton');
    editSubmitButton.addEventListener('click', async (event) => {
        event.preventDefault();

        const id = document.getElementById('editId').value;
        const flightNumber = document.getElementById('editFlightNumber').value;
        const departureAirportId = document.getElementById('editDepartureAirportId').value;
        const arrivalAirportId = document.getElementById('editArrivalAirportId').value;
        const departureDateTime = document.getElementById('editDepartureDateTime').value;
        const arrivalDateTime = document.getElementById('editArrivalDateTime').value;

        const updatedFlight = {
            id,
            flightNumber,
            departureAirportId,
            arrivalAirportId,
            departureDateTime,
            arrivalDateTime
        };

        try {
            await updateFlight(updatedFlight);
            location.reload();
            editFlightForm.style.display = 'none';
        } catch (error) {
            console.error('Failed to update flight:', error);
            location.reload();
        }
    });
}
