﻿using Airlines.Persistance.Models;
using Airlines.Persistence.Repositories;
using Airlines.Persistence.Repositories.Interfaces;

namespace Airlines.Business.Services;
public class AirlineService
{
    private readonly IAirlineRepository _airlineRepository;

    public AirlineService(IAirlineRepository airlineRepository)
    {
        _airlineRepository = airlineRepository;
    }

    public List<Airline> GetAirlines()
    {
        List<Airline> airlines = _airlineRepository.GetAirlines();
        return airlines;
    }

    public Airline GetAirlineById(int id)
    {
        Airline airline = _airlineRepository.GetAirlineById(id);
        return airline;
    }

    public List<Airline> GetAirlinesByAnyField(string text)
    {
        List<Airline> airline = _airlineRepository.GetAirlinesByAnyField(text);
        return airline;
    }

    public bool AddAirlineToDB(Airline airline)
    {
        bool success = _airlineRepository.AddAirline(airline);
        return success;
    }

    public int GetAirlinesCount()
    {
        return _airlineRepository.GetCount();
    }

    public List<Airline> SearchInAirline(string searchTerm)
    {
        return _airlineRepository.Search(searchTerm);
    }

    public bool UpdateAirline(Airline airline)
    {
        bool success = _airlineRepository.UpdateAirline(airline);
        return success;
    }

    public bool DeleteAirline(int id)
    {
        bool success = _airlineRepository.DeleteAirline(id);
        return success;
    }
}
