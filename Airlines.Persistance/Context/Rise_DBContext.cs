﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;
using Airlines.Persistance.Configuration;
using Microsoft.EntityFrameworkCore;

namespace Airlines.Persistance.Models;

public partial class Rise_DBContext : DbContext
{
    public Rise_DBContext()
    {
    }

    public Rise_DBContext(DbContextOptions<Rise_DBContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Airline> Airlines { get; set; }

    public virtual DbSet<Airport> Airports { get; set; }

    public virtual DbSet<Flight> Flights { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (!optionsBuilder.IsConfigured)
        {
            string connectionString = ConfigurationManager.GetConnectionString("RiseAirlines");
            optionsBuilder.UseSqlServer(connectionString);
        }
    }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Airline>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Airline__3214EC27BAC38A4D");
        });

        modelBuilder.Entity<Airport>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Airport__3214EC275F1A64AA");

            entity.Property(e => e.Code).IsFixedLength();
        });

        modelBuilder.Entity<Flight>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Flight__3214EC077BEFAC73");

            entity.Property(e => e.FlightNumber).IsFixedLength();

            entity.HasOne(d => d.ArrivalAirport).WithMany(p => p.FlightArrivalAirports).HasConstraintName("FK_ArrivalAirport");

            entity.HasOne(d => d.DepartureAirport).WithMany(p => p.FlightDepartureAirports).HasConstraintName("FK_DepartureAirport");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}