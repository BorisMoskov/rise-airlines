﻿using Airlines.Persistance.Models;


namespace Airlines.Persistence.Repositories.Interfaces;
public interface IAirlineRepository
{
    public List<Airline> GetAirlines();
    public Airline GetAirlineById(int id);
    public List<Airline> GetAirlinesByAnyField(string text);
    public bool AddAirline(Airline airline);
    public int GetCount();
    public List<Airline> Search(string searchTerm);
    public bool UpdateAirline(Airline airline);
    public bool DeleteAirline(int id);
}