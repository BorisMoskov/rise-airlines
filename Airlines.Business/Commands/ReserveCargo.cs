﻿
using Airlines.Business.Models;
using AirlinesBusiness;
using System.Diagnostics.CodeAnalysis;

namespace Airlines.Business.Commands;
[ExcludeFromCodeCoverage]
public class ReserveCargo(Dictionary<string, CargoAircraft> aircrafts, LinkedList<Flight> flights, string command) : ICommand
{
    public void Execute()
    {
        string[] commandParts;
        string flightIdentifier;
        double cargoWeight;
        double cargoVolume;

        try
        {
            commandParts = [.. command.Split(' ')];
            flightIdentifier = commandParts[0];
            cargoWeight = double.Parse(commandParts[1]);
            cargoVolume = double.Parse(commandParts[2]);
        }
        catch
        {
            Console.WriteLine("Incorrect command format");
            Console.WriteLine("<Flight Identifier> <Cargo Weight> <Cargo Volume>");
            return;
        }

        Flight? flight = CheckIfFlightExist(flights, flightIdentifier);

        if (flight != null)
        {
            foreach (var aircraft in aircrafts)
            {
                if (cargoWeight <= aircraft.Value.CargoWeight && cargoVolume <= aircraft.Value.CargoVolume)
                {
                    PrintInformation.PrintReservationInfo(flightIdentifier);
                    return;
                }
            }
            Console.WriteLine("Booked unsuccessfully");
        }
        else
        {
            Console.WriteLine("No such flight exists");
        }
    }

    internal static Flight? CheckIfFlightExist(LinkedList<Flight> flights, string identifier)
    {
        foreach (var flight in flights)
        {
            if (flight.Identifier == identifier)
            {
                return flight;
            }
        }
        return null;
    }
}