﻿using Airlines.Business.Services;
using Airlines.Persistance.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Airlines.API.Controllers;
[Route("api/[controller]")]
[ApiController]
public class AirlineController : ControllerBase
{
    private readonly AirlineService _airlineService;

    public AirlineController(AirlineService airlineService)
    {
        _airlineService = airlineService;
    }

    [HttpGet]
    public IActionResult GetAll()
    {
        List<Airline> airlineList = _airlineService.GetAirlines();

        if (airlineList.Count == 0)
        {
            return NotFound();
        }
        else if (airlineList == null)
        {
            return StatusCode(500, new { message = "Internal server error." });
        }

        return Ok(airlineList);
    }

    [HttpGet("{id}")]
    public IActionResult GetOne(int id)
    {
        var airline = _airlineService.GetAirlineById(id);

        if (airline == null)
        {
            return BadRequest($"Airline with id {id} was not found.");
        }

        return Ok(airline);
    }

    [HttpGet("filter/{filter}")]
    public IActionResult GetByFilter(string text)
    {
        var airline = _airlineService.GetAirlinesByAnyField(text);

        if (airline == null)
        {
            return BadRequest($"Airline was not found.");
        }

        return Ok(airline);
    }

    [HttpPost]
    public IActionResult Create([FromBody] Airline newAirline)
    {
        var isCreated = _airlineService.AddAirlineToDB(newAirline);

        if (!isCreated)
        {
            return StatusCode(500, new { message = "Internal server error." });
        }

        return CreatedAtAction(nameof(GetAll), new { id = newAirline.Id }, newAirline);
    }

    [HttpPut]
    public IActionResult Update([FromBody] Airline airline)
    {
        var isUpdated = _airlineService.UpdateAirline(airline);

        if (!isUpdated)
        {
            return StatusCode(500, new { message = "Internal server error." });
        }

        return Ok();
    }

    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
        var isDeleted = _airlineService.DeleteAirline(id);

        if (!isDeleted)
        {
            return BadRequest($"Airline with id {id} was already deleted or was not found.");
        }

        return Ok();
    }
}
