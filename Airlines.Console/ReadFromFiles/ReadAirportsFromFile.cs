﻿using Airlines.Business.Models;

namespace Airlines.Console.ReadFromFiles;
internal class ReadAirportsFromFile
{
    internal static Dictionary<string, Airport> ReadFromFile()
    {
        var filePath = "../../../InputData/AirportsData.csv";
        Dictionary<string, Airport> airports = [];

        using (var reader = new StreamReader(filePath))
        {
            // Skip the first line (header)
            _ = reader.ReadLine();

            string line;

            while ((line = reader.ReadLine()!) != null)
            {
                var parts = line.Split(',');

                var airport = new Airport(parts[0], parts[1], parts[2], parts[3]);
                _ = airports.TryAdd(airport.Identifier, airport);
            }
        }

        return airports;
    }
}
