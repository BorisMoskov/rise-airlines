﻿
using System.Diagnostics.CodeAnalysis;

namespace Airlines.Business.Commands;
[ExcludeFromCodeCoverage]
public class NewRoute() : ICommand
{
    private readonly RouteManagement _manager = new();
    public void Execute()
    {
        _manager.NewRoute();
        Console.WriteLine("A new flight route has been initialized");
    }
}