﻿using System.Diagnostics.CodeAnalysis;

namespace Airlines.Business.Commands;
[ExcludeFromCodeCoverage]
public class PrintRoute(RouteManagement manager) : ICommand
{
    private readonly RouteManagement _manager = manager;

    public void Execute() => _manager.PrintRoute();
}