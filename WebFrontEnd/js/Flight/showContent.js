document.addEventListener('DOMContentLoaded', function () {
    const toggleFormButton = document.getElementById('toggleButton');
    const showMoreButton = document.getElementById('showMore');
    const form = document.getElementById('flightForm');
    const table = document.querySelector('.table');

    form.style.display = 'none';

    toggleFormButton.addEventListener('click', function () {
        if (form.style.display === 'none') {
            form.style.display = 'flex';
            table.style.display = 'none';
            toggleFormButton.textContent = 'Show Table';
            showMoreButton.style.display = 'none';
        } else {
            form.style.display = 'none';
            table.style.display = 'flex';
            toggleFormButton.textContent = 'Show Form';
            showMoreButton.style.display = 'block';
        }
    });

    let showAll = false;
    const initialRowsDisplayed = 3;

    showMoreButton.addEventListener('click', function () {
        const tableRows = document.querySelectorAll('#flightTableBody tr');
        if (!showAll) {
            tableRows.forEach(row => {
                row.style.display = 'table-row';
            });
            showAll = true;
            showMoreButton.textContent = 'Show Less';
        } else {
            tableRows.forEach((row, index) => {
                row.style.display = index < initialRowsDisplayed ? 'table-row' : 'none';
            });
            showAll = false;
            showMoreButton.textContent = 'Show More';
        }
    });
});
