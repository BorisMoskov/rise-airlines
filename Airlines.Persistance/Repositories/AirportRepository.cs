﻿using Airlines.Persistance.Models;
using Airlines.Persistence.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Airlines.Persistence.Repositories;
public class AirportRepository : IAirportRepository
{
    private readonly Rise_DBContext _context;
    public AirportRepository(Rise_DBContext context)
    {
        _context = context;
    }

    public List<Airport> GetAirports()
    {
        return _context.Airports.ToList();
    }

    public Airport GetAirportById(int id)
    {
        try
        {
            return _context.Airports.FirstOrDefault(a => a.Id == id)!;
        }
        catch
        {
            return null!;
        }
    }

    public List<Airport> GetAirportByAnyField(string text)
    {
        IQueryable<Airport> query = _context.Airports;

        if (!string.IsNullOrEmpty(text))
        {
            query = query.Where(airport =>
                    airport.Name.Contains(text) ||
                    airport.Country.Contains(text) ||
                    airport.City.Contains(text) ||
                    airport.Code.Contains(text) ||
                    airport.Founded.ToString()!.Contains(text) ||
                    airport.RunwaysCount.ToString()!.Contains(text));
        }

        return query.ToList();
    }

    public bool AddAirport(Airport airport)
    {
        try
        {
            _ = _context.Airports.Add(airport);
            _ = _context.SaveChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }

    public bool UpdateAirport(Airport airport)
    {
        try
        {
            _ = _context.Airports.Update(airport);
            _ = _context.SaveChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }

    public bool DeleteAirport(int id)
    {
        try
        {
            var airport = _context.Airports.FirstOrDefault(a => a.Id == id);
            if (airport != null)
            {
                _ = _context.Airports.Remove(airport);
                _ = _context.SaveChanges();
                return true;
            }
            return false; // If the airport with the given ID doesn't exist
        }
        catch
        {
            return false;
        }
    }

    public int GetCount()
    {
        return _context.Airports.Count();
    }

    public List<Airport> Search(string searchTerm)
    {
        List<Airport> airports = _context.Airports.ToList();
        List<Airport> results = new List<Airport>();

        if (searchTerm == null)
        {
            return airports;
        }

        foreach (var airport in airports)
        {
            if (MatchesSearchTerm(airport, searchTerm))
            {
                results.Add(airport);
            }
        }

        return results;
    }

    public static bool MatchesSearchTerm(Airport airport, string searchTerm)
    {
        if (int.TryParse(searchTerm, out int runwaysCount))
        {
            // Search specifically in the RunwaysCount property
            return airport.RunwaysCount == runwaysCount;
        }
        else
        {
            // Default search: check if any property of the airline matches the search term
            return airport.Name!.Contains(searchTerm, StringComparison.OrdinalIgnoreCase) ||
                   airport.Country!.Contains(searchTerm, StringComparison.OrdinalIgnoreCase) ||
                   airport.City!.Contains(searchTerm, StringComparison.OrdinalIgnoreCase) ||
                   airport.Code!.Contains(searchTerm, StringComparison.OrdinalIgnoreCase) ||
                   airport.RunwaysCount.ToString()!.Contains(searchTerm, StringComparison.OrdinalIgnoreCase) ||
                   airport.Founded.ToString()!.Contains(searchTerm, StringComparison.OrdinalIgnoreCase);
        }
    }
}