﻿using Airlines.Business.Models;
using AirlinesBusiness;
using System.Diagnostics.CodeAnalysis;

namespace Airlines.Business.Commands;
[ExcludeFromCodeCoverage]
public class SearchSpecificData(Dictionary<string, Airport> airports, HashSet<string> airlines, LinkedList<Flight> flights, string command) : ICommand
{
    public void Execute()
    {
        string[] commandParts;
        string targetWord;
        try
        {
            // Split command into parts
            commandParts = command.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            targetWord = commandParts[1];
        }
        catch (Exception)
        {
            Console.WriteLine("Incorrect command format");
            Console.WriteLine("search <search term>");
            return;
        }

        // Search in airports
        if (SearchAirports(airports, targetWord))
        {
            PrintInformation.PrintSearchSpecificWordType("airports", targetWord);
        }
        else if (airlines.Contains(targetWord))
        {
            // Search in airlines
            PrintInformation.PrintSearchSpecificWordType("airlines", targetWord);
        }
        else if (SearchFlights(flights, targetWord))
        {
            // Search in flights
            PrintInformation.PrintSearchSpecificWordType("flights", targetWord);
        }
        else
        {
            //Not found
            PrintInformation.PrintSearchSpecificWordType("Not found", targetWord);
        }
    }
    internal static bool SearchAirports(Dictionary<string, Airport> airports, string targetWord)
    {
        return airports.ContainsKey(targetWord) ||
               airports.Values.Any(airport =>
                   airport.Name.Contains(targetWord, StringComparison.OrdinalIgnoreCase) ||
                   airport.City.Contains(targetWord, StringComparison.OrdinalIgnoreCase) ||
                   airport.Country.Contains(targetWord, StringComparison.OrdinalIgnoreCase));
    }
    internal static bool SearchFlights(LinkedList<Flight> flights, string targetWord)
    {
        return flights.Any(flight =>
                   flight.Identifier == targetWord ||
                   flight.DepartureAirport == targetWord ||
                   flight.ArrivalAirport == targetWord ||
                   flight.AircraftModel == targetWord);
    }
}
