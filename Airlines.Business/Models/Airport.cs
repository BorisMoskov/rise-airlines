﻿namespace Airlines.Business.Models
{
    public class Airport
    {
        private string _identifier;
        private string _name;
        private string _city;
        private string _country;

        public Airport(string identifier, string name, string city, string country)
        {
            if (!IsValidIdentifier(identifier))
                Console.WriteLine("Invalid identifier of the airport");

            if (!IsValidParameter(name))
                Console.WriteLine("Invalid name of the airport");

            if (!IsValidParameter(city))
                Console.WriteLine("Invalid city of the airport");

            if (!IsValidParameter(country))
                Console.WriteLine("Invalid country of the airport");

            _identifier = identifier;
            _name = name;
            _city = city;
            _country = country;
        }

        public string Identifier
        {
            get => _identifier;
            set
            {
                if (IsValidIdentifier(value))
                    _identifier = value;
                else
                    Console.WriteLine("Invalid identifier!");
            }
        }

        public string Name
        {
            get => _name;
            set
            {
                if (IsValidParameter(value))
                    _name = value;
                else
                    Console.WriteLine("Invalid name!");
            }
        }

        public string City
        {
            get => _city;
            set
            {
                if (IsValidParameter(value))
                    _city = value;
                else
                    Console.WriteLine("Invalid city!");
            }
        }

        public string Country
        {
            get => _country;
            set
            {
                if (IsValidParameter(value))
                    _country = value;
                else
                    Console.WriteLine("Invalid country!");
            }
        }

        public static bool IsValidIdentifier(string identifier)
        {
            if (identifier.Length is >= 2 and <= 4)
            {
                foreach (var symbol in identifier)
                    if (!char.IsLetterOrDigit(symbol))
                        return false;

                return true;
            }

            return false;
        }

        public static bool IsValidParameter(string parameter)
        {
            foreach (var symbol in parameter)
                if (!char.IsLetter(symbol) && symbol != ' ' && symbol != '.')
                    return false;
            return true;
        }
    }
}
