﻿namespace Airlines.Business.Models;
public class Flight(string identifier, string departureAirportCode, string arrivalAirportCode, string aircraftModel, double price, double time)
{
    public string Identifier { get; set; } = identifier;
    public string DepartureAirport { get; set; } = departureAirportCode;
    public string ArrivalAirport { get; set; } = arrivalAirportCode;
    public string AircraftModel { get; set; } = aircraftModel;
    public double Price { get; set; } = price;
    public double Time { get; set; } = time;
}
