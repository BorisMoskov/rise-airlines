﻿using System.Collections.Generic;
using System.IO;
using Airlines.Business.Models;

namespace Airlines.Console.ReadFromFiles
{
    internal class ReadAirlinesFromFile
    {
        internal static HashSet<string> ReadFromFile()
        {
            var filePath = "../../../InputData/AirlinesData.csv";
            var airlines = new HashSet<string>();

            using (var reader = new StreamReader(filePath))
            {
                // Skip the first line (header)
                _ = reader.ReadLine();

                string line;

                while ((line = reader.ReadLine()!) != null)
                {
                    var airlineName = line.Split(',')[0];
                    if (!Airline.IsValidName(airlineName))
                        continue;
                    _ = airlines.Add(airlineName);
                }
            }

            return airlines;
        }
    }
}
