using Airlines.Business.Services;
using Airlines.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace Airlines.Web.Controllers;
public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly AirlineService _airlineService;
    private readonly AirportService _airportService;
    private readonly FlightService _flightService;

    public HomeController(ILogger<HomeController> logger, AirlineService airlineService,
        AirportService airportService, FlightService flightService)
    {
        _logger = logger;
        _airlineService = airlineService;
        _airportService = airportService;
        _flightService = flightService;
    }

    public IActionResult Index()
    {
        int airlineCount = _airlineService.GetAirlinesCount();
        int airportCount = _airportService.GetAirportsCount();
        int flightCount = _flightService.GetFlightsCount();

        // Pass the counts to the view
        ViewBag.AirlineCount = airlineCount;
        ViewBag.AirportCount = airportCount;
        ViewBag.FlightCount = flightCount;

        return View();
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
