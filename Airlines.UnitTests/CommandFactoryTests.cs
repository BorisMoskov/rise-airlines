﻿using Airlines.Business.Commands;
using Airlines.Business.Models;
using Airlines.Business;

namespace Airlines.UnitTests;
public class CommandFactoryTests
{
    [Theory]
    [InlineData("search", typeof(SearchSpecificData))]
    [InlineData("sort", typeof(SortSpecificData))]
    [InlineData("exist", typeof(ExistAirline))]
    [InlineData("list", typeof(ListAllAirportsCityCountry))]
    public void CreateCommand_ValidCommands_Should_ReturnCorrectCommands(string command, Type expectedType)
    {
        var airports = new Dictionary<string, Airport>();
        var airportsByCity = new Dictionary<string, List<Airport>>();
        var airportsByCountry = new Dictionary<string, List<Airport>>();
        var airlines = new HashSet<string>();
        var flights = new LinkedList<Flight>();
        var manager = new RouteManagement();
        var cargoAircraftDictionary = new Dictionary<string, CargoAircraft>();
        var passengerAircraftDictionary = new Dictionary<string, PassengerAircraft>();
        var routeTree = new FlightRouteTree("DFW");
        var flightGraph = new Graph<string>();
        var fullCommand = $"{command} some_parameters";

        var result = CommandFactory.CreateCommand(airports, airportsByCity, airportsByCountry, airlines, flights, manager, cargoAircraftDictionary, passengerAircraftDictionary, routeTree, flightGraph, fullCommand);

        Assert.IsType(expectedType, result);
    }

    [Fact]
    public void CreateCommand_InvalidCommand_Should_ReturnNull()
    {
        var airports = new Dictionary<string, Airport>();
        var airportsByCity = new Dictionary<string, List<Airport>>();
        var airportsByCountry = new Dictionary<string, List<Airport>>();
        var airlines = new HashSet<string>();
        var flights = new LinkedList<Flight>();
        var manager = new RouteManagement();
        var cargoAircraftDictionary = new Dictionary<string, CargoAircraft>();
        var passengerAircraftDictionary = new Dictionary<string, PassengerAircraft>();
        var routeTree = new FlightRouteTree("DFW");
        var flightGraph = new Graph<string>();
        var fullCommand = "invalid_command some_parameters";

        var result = CommandFactory.CreateCommand(airports, airportsByCity, airportsByCountry, airlines, flights, manager, cargoAircraftDictionary, passengerAircraftDictionary, routeTree, flightGraph, fullCommand);

        Assert.Null(result);
    }

    [Fact]
    public void CreateCommand_SearchCommand_ReturnsSearchSpecificData()
    {
        var airports = new Dictionary<string, Airport>();
        var airlines = new HashSet<string>();
        var flights = new LinkedList<Flight>();
        var fullCommand = "search query";

        var command = CommandFactory.CreateCommand(airports, null!, null!, airlines, flights, null!, null!, null!, null!, null!, fullCommand);

        _ = Assert.IsType<SearchSpecificData>(command);
    }

    [Fact]
    public void CreateCommand_SortCommand_ReturnsSortSpecificData()
    {
        var airports = new Dictionary<string, Airport>();
        var airlines = new HashSet<string>();
        var flights = new LinkedList<Flight>();
        var fullCommand = "sort airports";

        var command = CommandFactory.CreateCommand(airports, null!, null!, airlines, flights, null!, null!, null!, null!, null!, fullCommand);

        _ = Assert.IsType<SortSpecificData>(command);
    }

    [Fact]
    public void CreateCommand_ExistCommand_ReturnsExistAirline()
    {
        var airlines = new HashSet<string>();
        var fullCommand = "exist airline";

        var command = CommandFactory.CreateCommand(null!, null!, null!, airlines, null!, null!, null!, null!, null!, null!, fullCommand);

        _ = Assert.IsType<ExistAirline>(command);
    }

    [Fact]
    public void CreateCommand_ListCommand_ReturnsListAllAirportsCityCountry()
    {
        var airportsByCity = new Dictionary<string, List<Airport>>();
        var airportsByCountry = new Dictionary<string, List<Airport>>();
        var fullCommand = "list city";

        var command = CommandFactory.CreateCommand(null!, airportsByCity, airportsByCountry, null!, null!, null!, null!, null!, null!, null!, fullCommand);

        _ = Assert.IsType<ListAllAirportsCityCountry>(command);
    }

    [Fact]
    public void CreateCommand_RouteCommandWithNew_ReturnsNewRoute()
    {
        var fullCommand = "route new";

        var command = CommandFactory.CreateCommand(null!, null!, null!, null!, null!, null!, null!, null!, null!, null!, fullCommand);

        _ = Assert.IsType<NewRoute>(command);
    }

    [Fact]
    public void CreateCommand_RouteCommandWithRemove_CallsManagerRemoveLastFlight()
    {
        var manager = new RouteManagement();
        var fullCommand = "route remove";

        var command = CommandFactory.CreateCommand(null!, null!, null!, null!, null!, manager, null!, null!, null!, null!, fullCommand);

        Assert.Null(command);
    }

    [Fact]
    public void CreateCommand_RouteCommandWithAdd_CallsManagerAddFlight()
    {
        var manager = new RouteManagement();
        var flights = new LinkedList<Flight>();
        var fullCommand = "route add Flight1";

        var command = CommandFactory.CreateCommand(null!, null!, null!, null!, flights, manager, null!, null!, null!, null!, fullCommand);

        Assert.Null(command);
    }

    [Fact]
    public void CreateCommand_RouteCommandWithPrint_ReturnsPrintRoute()
    {
        var manager = new RouteManagement();
        var fullCommand = "route print";

        var command = CommandFactory.CreateCommand(null!, null!, null!, null!, null!, manager, null!, null!, null!, null!, fullCommand);

        _ = Assert.IsType<PrintRoute>(command);
    }

    [Fact]
    public void CreateCommand_RouteCommandWithInvalidOption_ReturnsNull()
    {
        var fullCommand = "route invalid";

        var command = CommandFactory.CreateCommand(null!, null!, null!, null!, null!, null!, null!, null!, null!, null!, fullCommand);

        Assert.Null(command);
    }

    [Theory]
    [InlineData(new string[] { "A", "B", "C" }, "A", true)]
    [InlineData(new string[] { "A", "B", "C" }, "D", false)]
    public void ContainsNode_ReturnsExpectedResult(string[] vertices, string targetVertex, bool expectedResult)
    {
        var graph = new Graph<string>();
        foreach (var vertex in vertices)
        {
            graph.AddVertex(vertex);
        }

        var result = graph.ContainsNode(targetVertex);

        Assert.Equal(expectedResult, result);
    }

    [Theory]
    [InlineData(new string[] { "A", "B", "C" }, "A", "B", true)]
    [InlineData(new string[] { "A", "B", "C" }, "E", "D", false)]
    public void AddEdge_AddsEdgeBetweenVertices(string[] vertices, string source, string destination, bool expectedEdgeAdded)
    {
        var graph = new Graph<string>();
        foreach (var vertex in vertices)
        {
            graph.AddVertex(vertex);
        }

        graph.AddEdge(source, destination);
        var sourceNode = graph.GetNode(source);
        _ = graph.GetNode(destination);
        var edgeAdded = sourceNode?.Neighbours.Contains(destination) ?? false;

        Assert.Equal(expectedEdgeAdded, edgeAdded);
    }

    [Theory]
    [InlineData(new string[] { "A", "B", "C" }, "A", "B", true)]
    [InlineData(new string[] { "A", "B", "C" }, "A", "D", false)]
    public void PrintGraph_PrintsGraphCorrectly(string[] vertices, string source, string destination, bool expectedEdgePrinted)
    {
        var graph = new Graph<string>();
        foreach (var vertex in vertices)
        {
            graph.AddVertex(vertex);
        }
        graph.AddEdge(source, destination);

        var printedGraph = CaptureConsoleOutput(() => graph.PrintGraph(x => x));

        Assert.Contains(expectedEdgePrinted ? $"{source} -> {destination}" : $"{source} ->", printedGraph);
    }

    private static string CaptureConsoleOutput(Action action)
    {
        var consoleOutput = new StringWriter();
        Console.SetOut(consoleOutput);

        action.Invoke();

        return consoleOutput.ToString();
    }

    [Fact]
    public void AddFlightToRoute_Execute_AddsFlightToRoute()
    {
        var flights = new LinkedList<Flight>();
        _ = flights.AddLast(new Flight("FL123", "JFK", "LAX", "Boeing 747", 350, 6));

        var addFlightToRouteCommand = new AddFlightToRoute("route add FL123", flights);

        addFlightToRouteCommand.Execute();

        _ = Assert.Single(flights);
        Assert.Equal("FL123", flights.First!.Value.Identifier);
    }
}