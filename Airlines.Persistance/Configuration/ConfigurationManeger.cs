﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Persistance.Configuration;
public class ConfigurationManager
{
    private static readonly IConfigurationRoot _configuration;

    static ConfigurationManager()
    {
        // Build configuration once
        _configuration = new ConfigurationBuilder()
            .AddJsonFile("appSettings.json")
            .Build();
    }

    // Get connection string from configuration
    public static string GetConnectionString(string name)
        => _configuration.GetConnectionString(name)!;
}
